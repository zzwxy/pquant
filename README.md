# pquant

Quantum circuit simulation program in c++

## Purpose

Quantum Simulator targeting on c++ to easily accelarate the program

## TODO

Now I'm targeting on the simulation of Pauli noise and develop the algorithm test it's error rate.

I will also try to make the program parallel for large scale quantum simulation when necessary.
##TODO tomorrow

	Now the program could test arbitrary error rate on the anti-commutant of stabilizer group. It consumes 20min for one stabilizer group on 3qubits experiment.

	How to test the full Pauli group?

	How to detect the Pauli error for one particular logic gate?(e.g. CNOT gate)

## Collaborate with 

NIC group, DESY Zeuthen

Xiao yuan's group, Peking university

## Authors and acknowledgment
- [ ] [More information](https://faculty.pku.edu.cn/fengxu/zh_CN/xsxx/13365/content/1969.htm#xsxx)

## License

## Project status
Just started

