/*
	C++类模板及参数类型的运行时判断(typeid)
*/
 
#include <stdio.h>
#include <typeinfo>
#include <vector>
using namespace std;
 
template<class T>
class Exercise
{
	public:
		typedef T Type;
		typedef vector < Type > VT;
		Exercise(int n);
		void Display();
	private:
		VT dv;
};
 
template<class T>
Exercise<T>::Exercise(int n)
{
	T v;
	printf("type: %s\n", typeid(T).name());
	for(int i = 0; i < n; i++)
	{
		v = 1.1 * (i+1);
		dv.push_back(v);
	}
}
 
template<class T>
void Exercise<T>::Display()
{
	char fmt[2][10] = {"%d\t", "%.2f\t"};
	char *p = fmt[0];
	if(typeid(T) == typeid(double) || typeid(T) == typeid(float))
		p = fmt[1];
	for(typename vector<T>::iterator it = dv.begin(); it != dv.end(); it++)
	{
		printf(p, *it);
	}
	printf("\n\n");
}
 
int main()
{
	Exercise<double> ex1(5);
	ex1.Display();
 
	Exercise<float> ex2(5);
	ex2.Display();
 
	Exercise<int> ex3(5);
	ex3.Display();
	
	Exercise<long> ex4(5);
	ex4.Display();
 
 
	printf("\n");
	printf("%s\n", typeid(ex1).name());
	printf("%s\n", typeid(ex2).name());
	printf("%s\n", typeid(ex3).name());
	printf("%s\n", typeid(ex4).name());
 
	return 0;
}
