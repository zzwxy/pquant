
#include "pquant.h"


using namespace pquant;
using namespace Eigen;
using namespace std;


int main(){

	int Nqubit =7;
	
	bool if_show=false;
	sVector r=sVector::LinSpaced(ftoi(pow(2,Nqubit)),1,ftoi(pow(2,Nqubit)));

	quantum_circuit QC(Nqubit,r);

	noise_model NM(Nqubit);

	cout<<NM.N<<endl;

	pair<pairGateSite,float> pgsf11(pair<string, vector<int> >("Y",{1}),0.2);
	pair<pairGateSite,float> pgsf10(pair<string, vector<int> >("Id",{1}),0.8);

	pair<pairGateSite,float> pgsf01(pair<string, vector<int> >("X",{0}),0.2);
	pair<pairGateSite,float> pgsf00(pair<string, vector<int> >("Id",{0}),0.8);

	vectorNoiseInfo noise_info0{pgsf00,pgsf01};
	vectorNoiseInfo noise_info1{pgsf10,pgsf11};


	NM.add_quantum_noise("u1",{0},noise_info0);
	NM.add_quantum_noise("u2",{1},noise_info1);


	NM.add_measurement_noise({0,1,2,3,4},{0.3,0.1,0.1,0.1,0.1},{0.1,0.1,0.1,0.1,0.1});
	displayln_vector(NM.vec_p0to1);
	displayln_vector(NM.vec_p1to0);
	
	NM.display_noise_model_map(true);

	NM.clear_noise_model();

	NM.add_all_qubits_quantum_noise("u1",noise_info0);
	NM.add_all_qubits_measurement_noise(0.1,0.3);
	NM.display_noise_model_map(true);



	return 0;
}
