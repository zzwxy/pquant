
#include "pquant.h"


using namespace pquant;
using namespace Eigen;
using namespace std;

//example of run this program:
//g++ -I /usr/local/include/eigen3/Eigen/ -std=c++11 pauli_error_rate_estimation.cpp
//./a.out (-d) stabilizer/stabilizer_Z2.txt
inline int pair_list_to_site(vector<pairIB> index_list,int N){
	/*位点列表转化为整型
	 *index_list: {<{1,2},{true, false}>,<{0},{false}>,<{3},{true}>}
	 *对应computational basis 下的态矢量为|1010>即返回site值为10
	 *这里|1010>对应的qubit号为{3210},顺序与qiskit相同
	 */
	vector<bool> bool_list;
	int site=0;
	for(int i=0; i<N; i++ ){
		for(vector<pairIB>::iterator it=index_list.begin(); it !=index_list.end(); it++){
			int label;
			label=0;
			vector<int>::iterator first_it=(it->first).begin();
			for(; first_it != (it->first).end(); first_it++,label++){
				if(*first_it==i){
					break;
				}
			}
			if(first_it !=(it->first).end()){//不等则说明已经找到了这个数字i
				bool_list.push_back((it->second)[label]);
				break;
			}
		}
	}
	for(int i=0; i<N; i++){
		if (bool_list[i]==true){
			site+=ftoi(pow(2,i));
		}	
	}
	return site;
}
inline VectorXd Walsh_Hadamard(VectorXd svector, int N){
	Matrix<double,2,2> unitary;
	unitary<<1,1,1,-1;
	int d = ftoi(pow(2,N));
	
	const int size = ftoi(log2(unitary.rows()));
	const int res_size = N-size;

	for(int n_ind=0; n_ind<N; n_ind++){


		vector<int> site={n_ind};

		vector<int> res_site;

		for(int i=0; i<N; i++){
			vector<int>::iterator it = std::find(site.begin(),site.end(), i);//找site中是否有i
			if(it==site.end()){ //如果没有找到，则将此比特位放入 res_size
				res_site.push_back(i);
			}
		
		}
		//binary set of interacting qubits
		vector<vector<bool> > binary_list;

		//binary set of the rest qubits
		vector<vector<bool> >res_binary_list;
		
		const int d_int = ftoi(pow(2,size));
		const int d_unint = ftoi(pow(2,res_size));
		for(int i=0; i<d_int; i++){
			binary_list.push_back(itobinary(i,size));
		}

		VectorXd result_vector = VectorXd::Zero(d);

		if (res_size>0.1){
			for(int i=0; i<d_unint; i++){
				res_binary_list.push_back(itobinary(i,res_size));
			}
			for(int res_ind=0; res_ind<d_unint; res_ind++){
				pairIB res_pair = pairIB(res_site,res_binary_list[res_ind]);
				for(int ind=0; ind<d_int; ind++){
					pairIB int_pair = pairIB(site,binary_list[ind]);
					for(int ind_prod=0; ind_prod<d_int; ind_prod++){
						pairIB prod_pair = pairIB(site,binary_list[ind_prod]);
											
						vector<pairIB> result_list{
							res_pair, int_pair
						};
						vector<pairIB> svector_list{
							res_pair, prod_pair
						};
						//displayln_vector_pair_vector(result_list);
						result_vector(pair_list_to_site(result_list,N)) += unitary(ind,ind_prod)*svector(pair_list_to_site(svector_list,N));
					}
				}
			}

		}
		else
		{
			for(int ind=0; ind<d_int; ind++){
				pairIB int_pair = pairIB(site,binary_list[ind]);
				for(int ind_prod=0; ind_prod<d_int; ind_prod++){

						pairIB prod_pair = pairIB(site,binary_list[ind_prod]);

						vector<pairIB> result_list{
							int_pair
						};
						vector<pairIB> svector_list{
							prod_pair
						};
					result_vector(pair_list_to_site(result_list,N)) += unitary(ind,ind_prod)*svector(pair_list_to_site(svector_list,N));
				}
			}
		}


		svector=result_vector;
	}

	
	return svector/d;
}

inline bool symplectic_prod(vector<bool> a, vector<bool> b){
	int Nqubit = ftoi(a.size()/2.0);

	assert(a.size()==b.size());

	vector<bool> ax(a.begin(), a.begin()+Nqubit);
	vector<bool> az(a.begin()+Nqubit, a.begin()+2*Nqubit);
	vector<bool> bx(b.begin(), b.begin()+Nqubit);
	vector<bool> bz(b.begin()+Nqubit, b.begin()+2*Nqubit);

	return (ax*bz)^(az*bx);
}


int main(int argc, char **argv){
	
	get_start_time();
//////////////////set up////////////////////
	vector<int>  layer_list{1,2,3,4};
	//vector<int>  layer_list{1};
	int n_layer = layer_list.size();
	long shots = 5000;
	//long shots = 1;



////////////////////////////////////////////


	const OneqGate& Sx = const_g::get_Sx();
	const OneqGate& Sy = const_g::get_Sy();
	const OneqGate& Sz = const_g::get_Sz();




	////////////////////////////////////////////////////////////////////////////
	//set up according to stabilizer

	struct stabilizer_tableau *st;
	int param =0; //whether there are command-line parameters;
	if(argc==1) {
		displayln_error_info("stabilizer filename should be input");
		return 0;
	}

	if(argv[1][0]=='-') param=1;

	st = (struct stabilizer_tableau *)malloc(sizeof(struct stabilizer_tableau));

	if(param) read_stabilizer(st, argv[2],argv[1]);
	else read_stabilizer(st, argv[1],NULL);

	displayln_debug_info("initial stabilizer tableau");
	st->show_tableau();
	
	//create the gate list from the tableau
	st->generating_clifford_list();

	//simplify the clifford gate list
	st->gate_list_simplify(&st->logic_gate_list);

	vector<pairGateSite> state_prepare_gate_list = st->logic_gate_list; 

	//creat the reverse clifford list according to current logic_gate_list;
	st->reverse_clifford_gate_list();

	//simplify the reversed gate list.
	st->gate_list_simplify(&st->reverse_gate_list);

	vector<pairGateSite> measurement_gate_list = st->reverse_gate_list;



	stabilizer_tableau dest = get_destabilizer_group_generator(st);
	dest.show_tableau();


	int Nqubit = st->N;

	//构造高斯消元所需要的A矩阵,这个是标准(2N*2N)stabilizer tableau的转置,其一列代表一个PauliOperator
	Matrixb A;
	
	for(int q_i=0; q_i<Nqubit; q_i++){
		vector<bool> tmp;
		for(int q_j=0; q_j<Nqubit; q_j++)
			tmp.push_back((dest.vx)[q_j][q_i]);
				
		for(int q_j=0; q_j<Nqubit; q_j++)
			tmp.push_back((st->vx)[q_j][q_i]);

		A.push_back(tmp);
	}
	for(int q_i=0; q_i<Nqubit; q_i++){
		vector<bool> tmp;
		for(int q_j=0; q_j<Nqubit; q_j++)
			tmp.push_back((dest.vz)[q_j][q_i]);
				
		for(int q_j=0; q_j<Nqubit; q_j++)
			tmp.push_back((st->vz)[q_j][q_i]);

		A.push_back(tmp);
	}

	displayln_vector_vector(A);
	////////////////////////////////////////////////////////////////////////////
	//
	//build quantum circuit object
	quantum_circuit qc(Nqubit);
	qc.NOISY = true; 

	long long d = qc.d;
	long long A_G = d; //# of anticommutant
	long long C_G = d; //# of commutant




	//add quantum noise
	noise_model nm(Nqubit);

	float X_noise_prob = 0.2;

	vector<pair<string,float> > noise_info{pair<string,float>("X",X_noise_prob), pair<string,float>("Id",1-X_noise_prob)};

	nm.clear_noise_model();
	nm.add_all_qubits_quantum_noise("mock",noise_info);

	nm.display_noise_model_map(true);
	qc.implement_noise(nm);
	/////////////////////////////////////
	
	vector<vector<double> > w_h_m;
	for(int i_d=0; i_d<d; i_d++){
		vector<double> tmp(n_layer,0.0);
		w_h_m.push_back(tmp);
	}


	for(int layer_ind = 0; layer_ind<n_layer; layer_ind++){

		int layer = layer_list[layer_ind];

		displayln_debug_info(ssprintf("layer %d start",layer));

		for(int shot_ind=0; shot_ind<shots; shot_ind++){


			if(shot_ind%2000==0)
				displayln_debug_info(ssprintf("shots %d start",shot_ind));

			// construct the quantum circuit 
			qc.construct_by_gate_list(state_prepare_gate_list, false);//false: state prepare is noiseless
			//cout<<"state prepare gate list"<<endl;
			//qc.show_logic_gate_list();
			
			bool* a;
			vector<bool> a_list(2*Nqubit,0);

			for(int l_ind=0; l_ind < layer; l_ind++){
				qc.Channel();
				a = qc.Random_Pauli();
				vector<bool> a_tmp(a,a+2*Nqubit);
				delete []a;
				a_list = a_list^a_tmp;

				//displayln_vector(a_tmp);

			}


			//qc.show_all_gate_list();
			


			//if(shot_ind<3)
			//	qc.show_logic_gate_list();
			//qc.show_state_vector();

			qc.append_gate_list(measurement_gate_list, false);//false: measurement circuit is noiseless
			
			map<vector<bool>,long > counts = qc.measure(1);
			
			vector<bool> x_list = counts.begin()->first;
			//displayln_vector(bin_z);
			
			//bin_z.insert(bin_z.begin(), Nqubit, false);//insert 0 for X vector before the start of vector
			//如何自动判别测量结果对应的operator? 用stabilizer formalism ？我找到了，具体算法见Nielsen(IPad)p466的笔记

			vector<bool> bin_zx(Nqubit,false);
			vector<bool> bin_zz(Nqubit,false);
			int q_ind=0;
			for(vector<bool>::iterator it=x_list.begin(); it!=x_list.end(); it++){
				if(*it == true){
					bin_zx = bin_zx^dest.vx[q_ind]; // a_d+a_d'
					bin_zz = bin_zz^dest.vz[q_ind];
				}
				q_ind++;
			}
			vector_append(bin_zx, bin_zz);

			//怎样将a_list中的 commutant 部分剔除出去?简单，用高斯消元法解线性方程组即可
			vector<bool> x = binary_gaussian_solve(A, a_list); // dim-2*N vector, which is dest-st coefficient of decomposed a_list  

			//only the anti-commutant part is taken into account.
			vector<bool> bin_z(bin_zx);
			for(int q_i=0; q_i<Nqubit;q_i++){
				if(x[q_i]==true)
					bin_z = bin_z^col(A,q_i);// col(A,q_i) take the q_i-th column of A matrix 
			}
			



			
			//displayln_vector(bin_z);
			



			// V(X, G, t, m) //////////////////////////////////////////////////////////
			/*
			vector<pairGateSite> z_gate_list = st->bool_vector_to_Pauli_gate_list(bin_z);

			//displayln_vector_pair_string_vector(z_gate_list);
			
			//build quantum circuit object
			quantum_circuit qc_V(Nqubit);

			qc_V.NOISY = true; 

			qc_V.implement_noise(nm.noise_model_map);
			/////////////////////////////////////

			// construct the quantum circuit 
			qc_V.construct_by_gate_list(state_prepare_gate_list, false);//false: state prepare is noiseless
			qc_V.append_gate_list(z_gate_list);

			qc_V.append_gate_list(measurement_gate_list);
			//对这个qc的量子态计算所有Pauli operator的期望值
			//map<vector<bool>,long> counts = qc.measure(shots);

			// generate the Anticommutant set of noise operator
			vector<vector<int> > site_set;
			for(int i=0; i<C_G; i++){
				vector<bool> bin_i = itobinary(i,Nqubit);
				vector<int> site_v;
				int site_ind = Nqubit-1;
				for(vector<bool>::iterator it=bin_i.begin(); it!=bin_i.end(); it++){
					if(*it)
						site_v.push_back(site_ind);

					site_ind--;
				}
				site_set.push_back(site_v);

				Complex exp_result = qc_V.expect_overlap(Sz, site_v);  //XXX算符也是个稀疏矩阵，其乘法运算次数为N*2^N,而不是稠密矩阵的4^N   //TODO:将这里的expect_overlap换为包含统计误差的真正的量子测量
				//display_vector(site_v);
				//cout<<" exp = "<<exp_result<<endl;
				w_h_m[i][layer_ind] += exp_result.real();
				
			}
			*/
			//TODO:我已经完成了RunCB对所有stabilizer group的推广，还剩下V(X,G,t,m)没有完成推广，现在只能针对Pauli X error 有效，以及对WH变换的推广

			for(int i=0; i<C_G; i++){
				// generating the elements in CG, i.e. x\in X(see V's expression in the 2-rd step of V(X,G,t,m))
				vector<bool> bin_x(2*Nqubit,false);
				vector<bool> i_bin = itobinary(i,Nqubit);
				for(int j=0; j<Nqubit; j++){
					if(i_bin[j]){
						vector<bool> addition;
						vector_append(addition,st->vx[j]);
						vector_append(addition,st->vz[j]);
						bin_x = bin_x^addition;
					}
				}


				bool sign = symplectic_prod(bin_x, bin_z);
				if(sign)
					w_h_m[i][layer_ind] -= 1.0;
				else
					w_h_m[i][layer_ind] += 1.0;
			}


		}	//shots loop

		for(int i=0; i<C_G; i++)
			w_h_m[i][layer_ind] = w_h_m[i][layer_ind]/shots; //average the result
	} //layer loop


	displayln("w_h_m:");
	displayln_vector_vector(w_h_m);

	vector<double> ratio;

	Fit fit;
	for(vector<vector<double> >::iterator it_h=w_h_m.begin(); it_h != w_h_m.end(); it_h++){
		double v_h = (*it_h)[0];

		// normalize the sequence
		for(vector<double>::iterator it_m=(*it_h).begin(); it_m!=(*it_h).end(); it_m++ ){
			if(it_m ==(*it_h).begin())
				*it_m = 1;
			else
				*it_m /= v_h;
		}

		if(fit.linearFit(layer_list,log2_vector(*it_h)))
			ratio.push_back(fit.getFactor(1));
	}

	

	vector<double> f_h = 2^ratio;
	displayln("f_h:");
	displayln_vector(f_h);
	

	Map<VectorXd> f_h_tmp(f_h.data(),d);
	VectorXd error_rate = Walsh_Hadamard(f_h_tmp, Nqubit);

	cout<<"error rate:"<<endl<<error_rate.transpose()<<endl;

	cout<<"total time:  "<<get_total_time()<<endl;
	return 0;
}
