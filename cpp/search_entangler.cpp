

#include "pquant.h"


using namespace pquant;
using namespace Eigen;
using namespace std;

//example of run this program:
//./a.out (-d) stabilizer/stabilizer_X.txt

int main(int argc, char **argv){
	
	struct stabilizer_tableau *st;
	int param =0; //whether there are command-line parameters;
	if(argc==1) {
		displayln_error_info("stabilizer filename should be input");
		return 0;
	}

	if(argv[1][0]=='-') param=1;

	st = (struct stabilizer_tableau *)malloc(sizeof(struct stabilizer_tableau));

	int length;
	if(param) {
		read_stabilizer(st, argv[2],argv[1]);
		length = argv[3][0]-ASCII0;
	}	else {
		read_stabilizer(st, argv[1],NULL);
		length = argv[2][0]-ASCII0;
	}
	displayln_debug_info("initial stabilizer tableau");
	st->show_tableau();
	
	vector<vector<int> > sixary;
	int all = ftoi(pow(6,length));
	for(int i=0; i<all; i++ ){
		vector<int> six(length);
		vector<int>::iterator it=six.end();
		int j=i;
		while(j){
			it--;
			*it = j%6;
			j/=6;
		}
		sixary.push_back(six);

	}
	//displayln_vector_vector(sixary);

	cout<< "invariant clifford vector"<<endl;
	int flag = 0;
	for(auto it = sixary.begin(); it!=sixary.end(); it++){
		if(flag%100000==0)
			cout<<flag<<"finished."<<endl;
		flag++;
		vector<int> six_list = *it;
		stabilizer_tableau st_tmp(*st);
		for(auto it2 = six_list.begin(); it2!=six_list.end(); it2++){
			switch(*it2){
				case 0:
					st_tmp.H(0);
					break;
				case 1:
					st_tmp.H(1);
					break;
				case 2:
					st_tmp.P(0);
					break;
				case 3:
					st_tmp.P(1);
					break;
				case 4:
					st_tmp.CX(0,1);
					break;
				case 5:
					st_tmp.CX(1,0);
					break;
				default:
					{
					displayln_error_info("switch error");
					assert(false);
					}
			}
						

		}
		if(!st->compare_xz(st_tmp)){
			if(exist_eles(*it,{4,5})){
				if(!continue_ele(*it)){
					displayln_vector(*it);

				}
			}
		}


	}


	return 0;
}
