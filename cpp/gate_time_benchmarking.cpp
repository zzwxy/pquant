
#include "quantum_circuit.h"
#include "complex.h"
#include "matrix.h"
#include "timer.h"
#include "pqutil.h"
#include "show.h"

#include "Eigen/Dense"
#include <cmath>
#include <iostream>
#include <vector>
#include <algorithm>


using namespace pquant;
using namespace Eigen;
using namespace std;

int main(){

	const OneqGate& Sx = const_g::get_Sx();
	const OneqGate& Sy = const_g::get_Sy();
	const TwoqGate& CNOT = const_g::get_CNOT();	
	

	int Nqubit =2;
	

	//sVector r = sVector::Random(ftoi(pow(2,Nqubit)));
	sVector r=sVector::LinSpaced(ftoi(pow(2,Nqubit)),1,ftoi(pow(2,Nqubit)));

	quantum_circuit QC(Nqubit,r);

	sVector initial_vector = QC.state_vector;
	cout<<"initial vector"<<endl<<initial_vector<<endl;

	vector<int> site_0{0};//Sx gate interact at the 0-th qubit
	//int site_0;
	//site_0=0;

	sVector s_vector = QC.gate(Sx, site_0);
	

	cout<<"state vector = "<<endl<<s_vector<<endl;
	
	vector<int> site_01{0,1};//CNOT gate interact at the 0-1th qubit
	
	s_vector = QC.gate(CNOT,site_01);
	
	cout<<"state vector = "<<endl<<s_vector<<endl;
	

	///////////////////////////////////////////////////////////	
	//singe and two qubits time benchmarking
	///////////////////////////////////////////////////////////	
	
	//vector<int> Nqubit_list{15,16,17,18,19};
	vector<int> Nqubit_list{};
	
	int length = Nqubit_list.size();
	vector<double> Sx_time_list;
	vector<double> CNOT_time_list;
	
	for(int i=0; i<length;i++){
		Nqubit = Nqubit_list[i];
		r=sVector::LinSpaced(ftoi(pow(2,Nqubit)),1,ftoi(pow(2,Nqubit)));

		quantum_circuit QC_b(Nqubit,r);


		double t_Sx = get_time();
		QC_b.gate(Sx,site_0);
		t_Sx = get_time()-t_Sx;
		Sx_time_list.push_back(t_Sx);

		double t_CNOT = get_time();
		QC_b.gate(CNOT,site_01);
		t_CNOT = get_time()-t_CNOT;
		CNOT_time_list.push_back(t_CNOT);
		
		cout<<"Nqubit = "<<Nqubit_list[i]<<" finished"<<endl;
	}
	displayln_vector(Sx_time_list);
	displayln_vector(CNOT_time_list);





	return 0;
}
