#!/bin/bash
path="/Users/immanuel/Code/Alibaba/cpp/stabilizer/Q2_stabilizer_covering/stabilizer_"
suffix=".txt"
label_vec=(IZ IX YZ IY XZ)
#label_vec=(YZ)

shots=4000
#shots=10

#for var in `ls ./stabilizer/Q2_stabilizer_covering`

for((i=0;i<${#label_vec[@]};i++))
do 
	echo executing on $path${label_vec[$i]}$suffix
	./a.out $path${label_vec[$i]}$suffix ${label_vec[$i]} $shots
done
