
#include "pquant.h"


using namespace pquant;
using namespace Eigen;
using namespace std;

//example of run this program:
//./a.out (-d) stabilizer/stabilizer_X.txt 10000
inline int pair_list_to_site(vector<pairIB> index_list,int N){
	/*位点列表转化为整型
	 *index_list: {<{1,2},{true, false}>,<{0},{false}>,<{3},{true}>}
	 *对应computational basis 下的态矢量为|1010>即返回site值为10
	 *这里|1010>对应的qubit号为{3210},顺序与qiskit相同
	 */
	vector<bool> bool_list;
	int site=0;
	for(int i=0; i<N; i++ ){
		for(vector<pairIB>::iterator it=index_list.begin(); it !=index_list.end(); it++){
			int label;
			label=0;
			vector<int>::iterator first_it=(it->first).begin();
			for(; first_it != (it->first).end(); first_it++,label++){
				if(*first_it==i){
					break;
				}
			}
			if(first_it !=(it->first).end()){//不等则说明已经找到了这个数字i
				bool_list.push_back((it->second)[label]);
				break;
			}
		}
	}
	for(int i=0; i<N; i++){
		if (bool_list[i]==true){
			site+=ftoi(pow(2,i));
		}	
	}
	return site;
}
inline VectorXd Walsh_Hadamard(VectorXd svector, int N){
	Matrix<double,2,2> unitary;
	unitary<<1,1,1,-1;
	int d = ftoi(pow(2,N));
	
	const int size = ftoi(log2(unitary.rows()));
	const int res_size = N-size;

	for(int n_ind=0; n_ind<N; n_ind++){


		vector<int> site={n_ind};

		vector<int> res_site;

		for(int i=0; i<N; i++){
			vector<int>::iterator it = std::find(site.begin(),site.end(), i);//找site中是否有i
			if(it==site.end()){ //如果没有找到，则将此比特位放入 res_size
				res_site.push_back(i);
			}
		
		}
		//binary set of interacting qubits
		vector<vector<bool> > binary_list;

		//binary set of the rest qubits
		vector<vector<bool> >res_binary_list;
		
		const int d_int = ftoi(pow(2,size));
		const int d_unint = ftoi(pow(2,res_size));
		for(int i=0; i<d_int; i++){
			binary_list.push_back(itobinary(i,size));
		}

		VectorXd result_vector = VectorXd::Zero(d);

		if (res_size>0.1){
			for(int i=0; i<d_unint; i++){
				res_binary_list.push_back(itobinary(i,res_size));
			}
			for(int res_ind=0; res_ind<d_unint; res_ind++){
				pairIB res_pair = pairIB(res_site,res_binary_list[res_ind]);
				for(int ind=0; ind<d_int; ind++){
					pairIB int_pair = pairIB(site,binary_list[ind]);
					for(int ind_prod=0; ind_prod<d_int; ind_prod++){
						pairIB prod_pair = pairIB(site,binary_list[ind_prod]);
											
						vector<pairIB> result_list{
							res_pair, int_pair
						};
						vector<pairIB> svector_list{
							res_pair, prod_pair
						};
						//displayln_vector_pair_vector(result_list);
						result_vector(pair_list_to_site(result_list,N)) += unitary(ind,ind_prod)*svector(pair_list_to_site(svector_list,N));
					}
				}
			}

		}
		else
		{
			for(int ind=0; ind<d_int; ind++){
				pairIB int_pair = pairIB(site,binary_list[ind]);
				for(int ind_prod=0; ind_prod<d_int; ind_prod++){

						pairIB prod_pair = pairIB(site,binary_list[ind_prod]);

						vector<pairIB> result_list{
							int_pair
						};
						vector<pairIB> svector_list{
							prod_pair
						};
					result_vector(pair_list_to_site(result_list,N)) += unitary(ind,ind_prod)*svector(pair_list_to_site(svector_list,N));
				}
			}
		}


		svector=result_vector;
	}

	
	return svector/d;
}

inline bool symplectic_prod(vector<bool> a, vector<bool> b){
	int Nqubit = ftoi(a.size()/2.0);

	assert(a.size()==b.size());

	vector<bool> ax(a.begin(), a.begin()+Nqubit);
	vector<bool> az(a.begin()+Nqubit, a.begin()+2*Nqubit);
	vector<bool> bx(b.begin(), b.begin()+Nqubit);
	vector<bool> bz(b.begin()+Nqubit, b.begin()+2*Nqubit);

	return (ax*bz)^(az*bx);
}
inline bool symplectic_prod(vector<bool> ax, vector<bool> az, vector<bool> b){
	int Nqubit = ax.size();

	assert(ax.size() == az.size());
	assert(2*ax.size() == b.size());

	vector<bool> bx(b.begin(), b.begin()+Nqubit);
	vector<bool> bz(b.begin()+Nqubit, b.begin()+2*Nqubit);

	return (ax*bz)^(az*bx);
}


int main(int argc, char **argv){
	
	get_start_time();
//////////////////setup////////////////////
	vector<int>  layer_list{2,4,6,8,10};
	//vector<int>  layer_list{1,2};
	//vector<int>  layer_list{1};
	int n_layer = layer_list.size();
	long shots;
	//long shots = 1;



////////////////////////////////////////////


	const OneqGate& Sx = const_g::get_Sx();
	const OneqGate& Sy = const_g::get_Sy();
	const OneqGate& Sz = const_g::get_Sz();




	////////////////////////////////////////////////////////////////////////////
	//set up according to stabilizer

	struct stabilizer_tableau *st;
	int param =0; //whether there are command-line parameters;
	if(argc==1) {
		displayln_error_info("stabilizer filename should be input");
		return 0;
	}

	if(argv[1][0]=='-') param=1;

	st = (struct stabilizer_tableau *)malloc(sizeof(struct stabilizer_tableau));


	if(param) {
		read_stabilizer(st, argv[2],argv[1]);
		shots = atoi(argv[4]);
	}

	else {
		read_stabilizer(st, argv[1],NULL);
		shots = atoi(argv[3]);
	}

	st->init();

	displayln_debug_info("initial stabilizer tableau");
	st->show_tableau();

	
	st->DEBUG=false;
	//////////////////state preparation circuit desing/////////////////
	//create the gate list from the tableau
	st->generating_clifford_list();

	displayln("exchange info:");
	displayln_vector_pair(st->exchange_info);

	//simplify the clifford gate list
	st->gate_list_simplify(&st->logic_gate_list);

	vector<pairGateSite> state_prepare_gate_list = st->logic_gate_list; 

	//////////////////measurement circuit desing/////////////////
	//creat the reverse clifford list according to current logic_gate_list;
	st->reverse_clifford_gate_list();

	//simplify the reversed gate list.
	st->gate_list_simplify(&st->reverse_gate_list);

	vector<pairGateSite> measurement_gate_list = st->reverse_gate_list;



	stabilizer_tableau dest = get_destabilizer_group_generator(st);
	displayln_debug_info("destabilizer tableau:");
	dest.show_tableau();

	displayln("full r:");
	vector<bool> st_full_r=st->get_full_r();
	displayln_vector(st_full_r);
	displayln("full X:");
	displayln_vector_vector(st->get_full_X());
	displayln("full Z:");
	displayln_vector_vector(st->get_full_Z());

	int Nqubit = st->N;

	//构造高斯消元所需要的A矩阵,这个是标准(2N*2N)stabilizer tableau的转置,其一列代表一个PauliOperator
	Matrixb A;
	
	for(int q_i=0; q_i<Nqubit; q_i++){
		vector<bool> tmp;
		for(int q_j=0; q_j<Nqubit; q_j++)
			tmp.push_back((dest.vx)[q_j][q_i]);
				
		for(int q_j=0; q_j<Nqubit; q_j++)
			tmp.push_back((st->vx)[q_j][q_i]);

		A.push_back(tmp);
	}
	for(int q_i=0; q_i<Nqubit; q_i++){
		vector<bool> tmp;
		for(int q_j=0; q_j<Nqubit; q_j++)
			tmp.push_back((dest.vz)[q_j][q_i]);
				
		for(int q_j=0; q_j<Nqubit; q_j++)
			tmp.push_back((st->vz)[q_j][q_i]);

		A.push_back(tmp);
	}

	//displayln_vector_vector(A);
	////////////////////////////////////////////////////////////////////////////
	//
	//build quantum circuit object
	quantum_circuit qc(Nqubit);
	qc.NOISY = true; 

	long long d = qc.d;
	long long A_G = d; //# of anticommutant
	long long C_G = d; //# of commutant




	//add quantum noise
	noise_model nm(Nqubit);
	
	float CX_noise_prob = 0.1;
	
	pairGateSite noise_pair("CX",{0});
	pairGateSite Idle_pair("Id",{0});
	vectorNoiseInfo noise_info{pair<pairGateSite,float>(noise_pair,CX_noise_prob), pair<pairGateSite,float>(Idle_pair,1-CX_noise_prob)};

	nm.clear_noise_model();

	//nm.add_quantum_noise("CX",{1,0},noise_info);
	//Add noise only on the qc.Channel()
	nm.add_quantum_noise("mock",{0},noise_info);
	//Add Pauli noise:
	//nm.add_all_qubits_Pauli_noise("mock","X",0.005);
	





	nm.add_all_qubits_measurement_noise(0.02,0.03);//p0to1,p1to0
	nm.display_noise_model_map(true);
	qc.implement_noise(nm);
	/////////////////////////////////////
	
	vector<vector<double> > f_p_l;
	vector<vector<double> > f_p_l_error;
	for(int i_d=0; i_d<d; i_d++){
		vector<double> tmp(n_layer,0.0);
		 f_p_l.push_back(tmp);
		 f_p_l_error.push_back(tmp);
	}

	//////////////////////////////////////////////////////////////////////////////////////
	//   main experiment
	for(int layer_ind = 0; layer_ind<n_layer; layer_ind++){

		int layer = layer_list[layer_ind];

		displayln_debug_info(ssprintf("layer %d start",layer));

		for(int shot_ind=0; shot_ind<shots; shot_ind++){


			if(shot_ind%2000==0)
				displayln_debug_info(ssprintf("shots %d start",shot_ind));

			// construct the quantum circuit 
			qc.construct_by_gate_list(state_prepare_gate_list, false);//false: state prepare is noiseless
			//cout<<"state prepare gate list"<<endl;
			//qc.show_logic_gate_list();
			
			bool* a;
			vector<bool> a_list(2*Nqubit,0);

			stabilizer_tableau tem_st(*st); //Note (3.2)
			
			
			qc.Channel();

			for(int l_ind=0; l_ind < layer; l_ind++){
				a = qc.Random_Pauli();
				qc.Channel();
				qc.CX(0,1); 
				qc.CX(1,2);
				vector<bool> a_tmp(a,a+2*Nqubit);
				delete []a;
				for(int q_ind=0; q_ind <Nqubit;q_ind++){//
					tem_st.r[q_ind] = (tem_st.r)[q_ind]^symplectic_prod((tem_st.vx)[q_ind],(tem_st.vz)[q_ind],a_tmp);
				}
				
				tem_st.CX(0,1);// same as qc.CX(0,1);
				tem_st.CX(1,2);
				//displayln_vector(a_tmp);

			}
			a = qc.Random_Pauli();
			vector<bool> a_tmp(a,a+2*Nqubit);
			delete []a;
			for(int q_ind=0; q_ind < Nqubit;q_ind++){//
				tem_st.r[q_ind] = (tem_st.r)[q_ind]^symplectic_prod((tem_st.vx)[q_ind],(tem_st.vz)[q_ind],a_tmp);
			}
			

			//qc.show_all_gate_list();

			qc.append_gate_list(measurement_gate_list, false);//false: measurement circuit is noiseless
			
			map<vector<bool>,long > counts = qc.measure(1);
			
			vector<bool> one_result = counts.begin()->first;
			//displayln_vector(bin_z);
			
			//bin_z.insert(bin_z.begin(), Nqubit, false);//insert 0 for X vector before the start of vector
			//如何自动判别测量结果对应的operator? 用stabilizer formalism ？我找到了，具体算法见Nielsen(IPad)p466的笔记
			//displayln_vector(one_result);//Note (3.1)
			//displayln_vector(st->r);
			
			vector<bool> P_eigenvalue=one_result^(st->r);

			for(vector<pair<int,int> >::iterator it=st->exchange_info.begin(); it!=st->exchange_info.end(); it++)
				swap(P_eigenvalue[it->first], P_eigenvalue[it->second]);

			
			P_eigenvalue = P_eigenvalue^tem_st.r;
			//displayln_vector(P_eigenvalue);
		
			vector<bool> full_P_eigenvalue=st_full_r;//binary in full_P_eigenvalue correspond to the eigenvalue of each operator in a row of full_X and full_Z
			//displayln_vector(full_P_eigenvalue);
			for(int i=0; i<d;i++){
				vector<bool> binary=itobinary(i,Nqubit);
				for(int j=0; j<Nqubit;j++){
					if(binary[j])
						full_P_eigenvalue[i]=full_P_eigenvalue[i]^P_eigenvalue[j];
				}
				if(full_P_eigenvalue[i])
					 f_p_l[i][layer_ind]-=1.0;
				else
					 f_p_l[i][layer_ind]+=1.0;
			}
			//displayln_vector(full_P_eigenvalue);
			

		}//shots loop
		//calculate expectation value of each operator
		for(int i=0; i<C_G; i++){
			 f_p_l[i][layer_ind] = f_p_l[i][layer_ind]/shots; //average the result
			 f_p_l_error[i][layer_ind]=sqrt((1-pow(f_p_l[i][layer_ind],2))/(shots-1));
		}

	}//layer loop

	displayln("eigenvalues:");
	displayln_vector_vector( f_p_l);

	ofstream fout;


	char label[20];
	if(param) strcpy(label,argv[3]);
	else strcpy(label,argv[2]);

	fout.open(ssprintf("../data/p_estimation_eigenvalue_%dQ_%dlayer_%dshots_%s.txt",Nqubit,n_layer,shots,label));

	for (int i = 1; i < C_G; i++){
		for(int layer_ind=0; layer_ind<n_layer;layer_ind++){
			fout<<setprecision(8) << f_p_l[i][layer_ind]<<" ";
		}
		fout<<endl;
	}
	for (int i = 1; i < C_G; i++){
		for(int layer_ind=0; layer_ind<n_layer;layer_ind++){
			fout<<setprecision(8) << f_p_l_error[i][layer_ind]<<" ";
		}
		fout<<endl;
	}

	fout.close();

	vector<double> ratio;

	Fit fit;
	for(vector<vector<double> >::iterator it_h= f_p_l.begin(); it_h != f_p_l.end(); it_h++){
		double v_h = (*it_h)[0];

		// normalize the sequence
		for(vector<double>::iterator it_m=(*it_h).begin(); it_m!=(*it_h).end(); it_m++ ){
			if(it_m ==(*it_h).begin())
				*it_m = 1;
			else
				*it_m /= v_h;
		}

		if(fit.linearFit(layer_list,log2_vector(*it_h)))
			ratio.push_back(fit.getFactor(1));
	}

	

	vector<double> f_h = 2^ratio;
	displayln("f_h:");
	displayln_vector(f_h);

	fout.open(ssprintf("../data/p_estimation_f_%dQ_%dlayer_%dshots_%s.txt",Nqubit,n_layer,shots,label));

	for (int i = 1; i < d; i++){
		fout<<setprecision(8) <<f_h[i]<<" ";
	}

	fout.close();



	return 0;
}
