#include "pquant.h"

using namespace pquant;
using namespace Eigen;
using namespace std;

int main(int argc, char** argv){
	int Nqubit = 3;
	quantum_circuit qc(Nqubit);
	qc.NOISY = false;
	long long d = qc.d;

	vector<double> beta_list = {0.2,0.4,0.6,0.8,1.0,1.2,1.4,1.6,1.8,2.0,2.2,2.4,2.6,2.8,3.0};

	int n_beta=beta_list.size();



	long shots=10000;

	vector<double> g_beta_list;
	vector<double> g_beta_error_list;


	for(int beta_ind=0; beta_ind<n_beta; beta_ind++){
		
		double beta = beta_list[beta_ind];

		displayln_debug_info(ssprintf("beta= %.3f start",beta));


		vector<string> gate_list={"IXI","III","XII","XXI","XIX","IXX","IIX","XXX"};
		
		double Z=pow(E,4*beta)+pow(E,-4*beta)+6;
		vector<double> prob_list={pow(E,4*beta),1,1,1,1,1,1,pow(E,-4*beta)};
		prob_list = prob_list/Z;


		vectorNoiseInfo flip_probability = {};
		for(int i=0; i<d;i++){
			pairGateSite p_gs(gate_list[i],{2,1,0});
			flip_probability.push_back(pair<pairGateSite,double>(p_gs,prob_list[i]));
		}

		double exp_Z0=0;
		for(int shot_ind=0; shot_ind<shots; shot_ind++){

			if(shot_ind%2000==0)
				displayln_debug_info(ssprintf("shots %d start",shot_ind));

			qc.state_reset();

			qc.gate_set_with_probability(flip_probability);

			// measurement
			map<vector<bool>,long > counts = qc.measure(1);
			vector<bool> one_result = counts.begin()->first;

			if(one_result[1])
				exp_Z0-=1;
			else
				exp_Z0+=1;

		}
		exp_Z0=exp_Z0/shots;

		double exp_Z0_error = sqrt( (1-pow(exp_Z0,2))/(shots-1));

		displayln(ssprintf("<Z_0>=%.3f",exp_Z0));

		double g_beta = atanh(-exp_Z0)/2;
		double g_beta_error = abs( atanh(-(exp_Z0+exp_Z0_error))/2-g_beta );

		g_beta_list.push_back(g_beta);
		g_beta_error_list.push_back(g_beta_error);
	}


	ofstream fout;


	fout.open(ssprintf("./data/g_function_%dQ_%dbeta_%dshots.txt",Nqubit,n_beta,shots));

	for(int beta_ind=0; beta_ind<n_beta;beta_ind++){
			fout<<setprecision(8) << g_beta_list[beta_ind]<<" ";
		}
	fout<<endl;
	for(int beta_ind=0; beta_ind<n_beta;beta_ind++){
			fout<<setprecision(8) << g_beta_error_list[beta_ind]<<" ";
		}
	fout<<endl;

	fout.close();

	displayln_vector(g_beta_list);
	displayln_vector(g_beta_error_list);

	return 0;
}
