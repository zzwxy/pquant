#pragma once
#include <cmath>
#include <map>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
using namespace std; 

namespace pquant
{
////////////////////////////////////////////////////////////////////////
//////////////////////   constants   /////////////////////
const double PI = 3.1415926535898;
const double E =  2.7182818284590;



////////////////////////////////////////////////////////////////////////
//////////////////////   map extension function    /////////////////////
template<class T,class K>
inline vector<T> key_map(map<T,K> Map){
	vector<T> key;
	for(typename map<T,K>::iterator it=Map.begin(); it!=Map.end(); it++){
		key.push_back(it->first);
	}
	return key;
}
template<class T,class K>
inline vector<K> value_map(map<T,K> Map){
	vector<K> value;
	for(typename map<T,K>::iterator it=Map.begin(); it!=Map.end(); it++){
		value.push_back(it->second);
	}
	return value;
}

////////////////////////////////////////////////////////////////////////
//
////////////////////////////////////////////////////////////////////////
/////////////////////////// vector function///////////////////////
template<class T>
inline vector<double> log_vector(vector<T> vec, double base){
	vector<double> log_v;
	for(typename vector<T>::iterator it=vec.begin(); it!=vec.end(); it++)
		log_v.push_back(log(*it)/log(base));
	
	return log_v;
}

template<class T>
inline vector<double> log_vector(vector<T> vec){
	return log_vector(vec,exp(1));
}
template<class T>
inline vector<double> log2_vector(vector<T> vec){
	return log_vector(vec,2.0);
}
template<class T>
inline vector<double> log10_vector(vector<T> vec){
	return log_vector(vec,10.0);
}



template<class T,class K>
inline vector<double> operator^(T c, vector<K> vec){
	vector<double> result;
	for(typename vector<K>::iterator it=vec.begin(); it!=vec.end(); it++)
		result.push_back(pow((double)c, (double)(*it)));
	
	return result;
}


template<class T,class K>
inline vector<double> operator+(T c, vector<K> vec){
	vector<double> result;
	for(typename vector<K>::iterator it=vec.begin(); it!=vec.end(); it++)
		result.push_back((double) c + (double)(*it));
	
	return result;
}
template<class T,class K>
inline vector<double> operator+(vector<K> vec, T c){
	
	return c+vec;
}
template<class T,class K>
inline vector<double> operator-(T c, vector<K> vec){
	vector<double> result;
	for(typename vector<K>::iterator it=vec.begin(); it!=vec.end(); it++)
		result.push_back((double) c - (double)(*it));
	
	return result;
}
template<class T,class K>
inline vector<double> operator-(vector<K> vec, T c){
	vector<double> result;
	for(typename vector<K>::iterator it=vec.begin(); it!=vec.end(); it++)
		result.push_back((double)(*it) - (double)c);
	
	return result;
}


//scalar division
template<class T,class K>
inline vector<double> operator/(vector<T> vec, K c){
	vector<double> result;
	for(typename vector<T>::iterator it=vec.begin(); it!=vec.end(); it++)
		result.push_back((double)(*it)/(double)c);
	
	return result;
}
template<class T>
inline vector<T> col(vector<vector<T> >A, int c){
	/*return the c-th column of Matrix A
	 */
	vector<T> column;
	for(typename vector<vector<T> >::iterator it=A.begin(); it!=A.end(); it++)
		column.push_back((*it)[c]);
	
	return column;
}

////////////////////////////////////////////////////////////////////////
// vector Eigen converting  function


////////////////////////////////////////////////////////////////////////
// vector append  function
template<class T>
inline void vector_append(vector<T>& vec, vector<T>& addition){
	/*append 'addition' behind 'vec'
	 */
	for(typename vector<T>::iterator it = addition.begin(); it!=addition.end(); it++)
		vec.push_back(*it);
	
}


////////////////////////////////////////////////////////////////////////
// vector search  function

template<class T>
inline bool find_ele(vector<T> vec, T ele){
	typename vector<T>::iterator iter=find(vec.begin(),vec.end(),ele);
    if ( iter==vec.end())
		return false;//没找到
    else
		return true; //找到了
}

template<class T>
inline bool exist_eles(vector<T> vec, vector<T> eles){  
	//if there exist ele\in eles belong to vec, return 1, else return 0 
	for(typename vector<T>::iterator it = eles.begin(); it!=eles.end(); it++){

	typename vector<T>::iterator iter=find(vec.begin(),vec.end(),*it);
    if ( iter!=vec.end())
		return true;//找到了
	}

	return 0;  //there is no element in eles existed in vec
}

template<class T>
inline bool continue_ele(vector<T> vec){  
	//if there exist two or more continue element in vec, return 1, else return 0 
	T pre=vec[0];
	for(typename vector<T>::iterator it =vec.begin(); it!=vec.end();){
		it++;
		if(pre == *it)
			return true;

		pre = *it;
	}

	return 0;  //there is no element in eles existed in vec
}
////////////////////////////////////////////////////////////////////////
// binary vector function

inline vector<bool> operator^(vector<bool> a, vector<bool> b){
	vector<bool>::iterator it_a = a.begin();
	vector<bool>::iterator it_b = b.begin();
	for(;it_a!=a.end();it_a++,it_b++)
		*it_a = (*it_a)^(*it_b);


	return a;
}

inline bool operator*(vector<bool> a, vector<bool> b){
	bool result =0;
	vector<bool>::iterator it_a = a.begin();
	vector<bool>::iterator it_b = b.begin();

	for(;it_a!=a.end(); it_a++,it_b++)
		result = result^(*it_a && *it_b);

	return result;
}
inline bool compare_vectorb(vector<bool> a, vector<bool> b){
	vector<bool>::iterator it_a = a.begin();
	vector<bool>::iterator it_b = b.begin();
	for(;it_a!=a.end(); it_a++,it_b++){
		if(*it_a ^ *it_b)
			return true;
	}
	return false; //return false means have no differenc
	
}
inline vector<bool> Vectorb_Zero(int size){
	vector<bool> tmp(size,0);
	return tmp;
}
inline vector<vector<bool> > Matrixb_Zero(int size){
	//false bool matrix with scale size*size
	vector<vector<bool> > Zero;

	for(int N_ind=0; N_ind<size; N_ind++){
		vector<bool> tmp(size,0);
		Zero.push_back(tmp);
	}
	return Zero;
}
inline vector<vector<bool> > Matrixb_Id(int size){
	vector<vector<bool> > Id;

	for(int N_ind=0; N_ind<size; N_ind++){
		vector<bool> tmp(size,0);
		tmp[N_ind] = true;
		Id.push_back(tmp);
	}
	return Id;
}
inline void rowswap(vector<vector<bool> >& A, int i, int k){
	vector<bool> tmp=A[i];
	A[i] = A[k];
	A[k] = tmp;
}

inline vector<bool> binary_gaussian_solve(vector<vector<bool> > A, vector<bool> b){
	/* return x in Ax=b 
	 */
	

	int size = b.size();
	//extend the matrix A
	for(int k=0; k<size; k++)
		A[k].push_back(b[k]);

	for(int k=0; k<size-1; k++){
		if(A[k][k]!=true){
			for(int i=k+1; i<size; i++){
				if(A[i][k]==true){
					rowswap(A, i, k);
					break;
				}
			}
		}
		for(int i=k+1;i<size;i++){
			if(A[i][k]== true){
				A[i] = A[i]^A[k];  //Gaussian elimination
			}
		}
	}
	vector<bool> x(size,false);
	for(int k=size-1; k >= 0; k--){
		x[k] = A[k][size];
		for(int i=k+1; i<size; i++)
			x[k] = x[k]^(A[k][i] && x[i]);

	}

	return x;
}
/* // using binary_gaussian_solve
#include<iostream>
#include "fitting.h"
#include "pquant.h"
#include<vector>
using namespace std;
using namespace pquant;
int main(){

	vector<vector<bool> > A={{1,0,0,1},{0,0,0,1},{0,0,1,0},{0,1,1,0}};
	vector<bool> b ={1,0,1,0};
	

	vector<bool> x = binary_gaussian_solve(A,b);
	displayln_vector(x);

	return 0;
}
*/

////////////////////////////////////////////////////////////////////////
//////////// swap function  ////////////////////////////////////////////
template<class T>
void swap(T& x, T& y)
{
   int temp;
   temp = x; /* 保存地址 x 的值 */
   x = y;    /* 把 y 赋值给 x */
   y = temp; /* 把 x 赋值给 y  */
  
   return;
}





////////////////////////////////////////////////////////////////////////
}
