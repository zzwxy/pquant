#pragma once
#include <random>


namespace pquant
{


//std::default_random_engine RANDOM;//RANDOM是我定义的一个随机数发生引擎，由此得到的随机数将使每次运行得到的结果相同(一次编译后每次运行a.out文件结果都不相同！)，如果想获得不同的结果的话，可以将这句话改为下面这句：
//std::default_random_engine RANDOM(time(0));//以时间作为随机数种子
unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
std::default_random_engine RANDOM(seed);//更高级的随机数种子

inline bool** random_01(double p, const int col, const int row){
	/*return one dimensional random 010101 array	
	 * p: 	success(get true or 1) probability
	 * col: column of the array
	 * row: row of the array
	 * random: random_eigine
	 * similar to np.random.randint(0, 2, p)*2-1 in python
	 */

	std::bernoulli_distribution dis_ber(p);

	//c++不允许动态地声明一个二维数组，所以下面的语法会报错：
	//auto bernoulli = new bool[row][col];
	bool** bernoulli = new bool*[row];
	for(int i = 0; i < row; ++i)
		bernoulli[i] = new bool[col];

	for(int ind_r=0; ind_r<row; ind_r++){
		for(int ind_c=0; ind_c<col; ind_c++){
			bernoulli[ind_r][ind_c] = (bool)dis_ber(RANDOM);
		}
	}
	return bernoulli;
}

inline bool* random_01(double p, const int col){
	/*return one dimensional random 010101 array	
	 * p: 	success(get true or 1) probability
	 * col: length of the array
	 * random: random eigine
	 * similar to np.random.randint(0, 2, p)*2-1 in python
	 */


	std::bernoulli_distribution dis_ber(p);

	auto bernoulli = new bool[col];

	for(int ind_c=0; ind_c<col; ind_c++){
		bernoulli[ind_c] = (bool)dis_ber(RANDOM);
	}
	return bernoulli;

}

inline bool** random_uniform_01(const int col, const int row){

	return random_01(0.5,col,row);
}

inline bool* random_uniform_01(const int col){

	return random_01(0.5,col);
}

inline vector<bool> random_sample01_from_1prob_vec(vector<float> prob_vec){
	/* return a list if (0,1) random number is larger than the elements in prob_vec
	 * eg. input{0.99,0.001}
	 * output{1,0}
	 */
	std::uniform_real_distribution<float> dis_float(0,1);
	vector<bool> sample;
	float epsilon;
	for(auto it=prob_vec.begin(); it!=prob_vec.end(); it++){
		epsilon = dis_float(RANDOM);
		if(epsilon<*it)
			sample.push_back(1);
		else 
			sample.push_back(0);
	}
	return sample;
}


}
