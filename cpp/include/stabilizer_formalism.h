#pragma once

#include "complex.h"
#include "matrix.h"
#include "timer.h"
#include "pqutil.h"
#include "show.h"
#include "random.h"
#include "noise_model.h"
#include "quantum_circuit.h"

#include "Eigen/Dense"
#include <cmath>
#include <iostream>
#include <vector>
#include <algorithm>
#include <string>

using namespace pquant;
using namespace Eigen;
using namespace std;

#define ASCII0 48

typedef vector<vector<bool> > Matrixb;

namespace pquant
{
inline bool bin_inner_prod(vector<bool> bin){
	bool result=0;
	for(vector<bool>::iterator it=bin.begin(); it!=bin.end(); it++)
		result ^= *it;

	return result;
}

struct stabilizer_tableau{
	int N; //# of qubits
	long long d;
	Matrixb vx; //N*N binary matrix for X
	Matrixb vz; //N*N binary matrix for Z
	vector<bool> r; //false == 0 == +
	vector<pairGateSite> reverse_gate_list;	
	vector<pairGateSite> logic_gate_list;  //stabilizer circuit

	bool DEBUG=false;

	vector<pair<int,int> > exchange_info;

	/////////////////////// 对象初始化 ////////////////////////////////////////////
	stabilizer_tableau(const stabilizer_tableau& data){
		vx = data.vx;
		vz = data.vz;
		r = data.r;
		logic_gate_list = data.logic_gate_list;
		reverse_gate_list = data.reverse_gate_list;
		N = data.N;
		exchange_info=data.exchange_info;
	}

	stabilizer_tableau(const int& Nqubit){
		//if only Number of qubits input, the standard stabilizer tableau will created.
		N = Nqubit;
		vx = Matrixb_Zero(Nqubit);
		vz = Matrixb_Id(Nqubit);
		r = Vectorb_Zero(Nqubit);
	}

	void init(){
		d = ftoi(pow(2,N));
	}

	////////////////////////////////////////////////////////////////


	////////////////////////////////////////////////////////////////
	/////////////////// stabilizer gate on tableau /////////////////
	inline void CX(int control, int target){
		assert(control<N);
		assert(target<N);
		vector<int> site_v{target,control};
		CX(site_v);

	}
	inline void CX(vector<int> site_v){
		
		string gate_name="CX";
		logic_gate_list.push_back(pairGateSite(gate_name, site_v));

		int c = N-1-site_v[1];
		int t = N-1-site_v[0];
		Matrixb::iterator itx=vx.begin();
		Matrixb::iterator itz=vz.begin();
		vector<bool>::iterator itr=r.begin();
		while(itx!=vx.end()){
			if((*itx)[c]) (*itx)[t] = (*itx)[t]^1;
			if((*itz)[t]) (*itz)[c] = (*itz)[c]^1;

			if( (*itx)[c] && (*itz)[t] && !( (*itx)[t] ^ (*itz)[c] ))
				(*itr) = !(*itr);

			itx++,itz++,itr++;
		}
		return;
	}

	inline void P(int site){
		assert(site<N);
		vector<int> site_v{site};
		P(site_v);
	}
	inline void P(vector<int> site_v){
		string gate_name="P";
		logic_gate_list.push_back(pairGateSite(gate_name, site_v));

		int s = N-1-site_v[0];
		Matrixb::iterator itx=vx.begin();
		Matrixb::iterator itz=vz.begin();
		vector<bool>::iterator itr=r.begin();
		while(itx!=vx.end()){
			if((*itx)[s] && (*itz)[s] ) (*itr) = !(*itr);

			(*itz)[s] = (*itz)[s] ^ (*itx)[s];

			itx++,itz++,itr++;
		}
		return;
		
	}


	inline void H(int site){
		assert(site<N);
		vector<int> site_v{site};
		H(site_v);
	}
	
	inline void H(vector<int> site_v){
		string gate_name="H";
		logic_gate_list.push_back(pairGateSite(gate_name, site_v));

		int s = N-1-site_v[0];
		Matrixb::iterator itx=vx.begin();
		Matrixb::iterator itz=vz.begin();
		vector<bool>::iterator itr=r.begin();
		bool tmp;
		while(itx!=vx.end()){
			tmp = (*itx)[s];
			(*itx)[s] = (*itz)[s];
			(*itz)[s] = tmp;
			if((*itx)[s] && (*itz)[s]) (*itr) = !(*itr);

			itx++,itz++,itr++;
		}
		return;
	}
	////////////////////////////////////////////////////////////////
	/////////////////   functional  ////////////////////////////////
	inline void generating_clifford_list(){
		/*find the clifford gate list to generate the current stabilizer tableau for the standard initial tableau,
		 *The step is a simplified adaption from Aaronson's <Improved Simulation of Stabilizer Circuits>
		 *As the whole purpose is just state preparation, we discard the distabilizer and only need focus on stabilizer part.
		 */
		
		Matrixb origin_vx=vx; //N*N binary matrix for X
		Matrixb origin_vz=vz; //N*N binary matrix for Z
		vector<bool> origin_r=r;

		logic_gate_list.clear();

		//BUG: step 1 are quite arbitrary, it is valid for my example, but I don't know if it work for general case
		//step 1:-H- make X matrix full rank
		//first we set X matrix has 1 on the diagonal
		for(int i_q_row=0;i_q_row<N;i_q_row++){
			if(!vx[i_q_row][i_q_row]){
				//if the diagonal term is zero, we find the first 1 below it and exchange these two rows
				int i_q_below=i_q_row;
				while(vx[i_q_below][i_q_row]!=1){
					i_q_below++;
					if(i_q_below==N){
						H(N-1-i_q_row);
						break;
					}
				}
				//exchange these two row
				if(i_q_below!=N){
					row_exchange(i_q_below,i_q_row);
					exchange_info.push_back(pair<int,int>(i_q_below,i_q_row));
				}
			}
		}
		if(DEBUG){
			displayln_debug_info("step 1:");
			show_tableau();
		}
		//step 2:-CX- make X matrix diagonal  CX作用的本质是列加，而高斯消元法要求行加，由于矩阵是满秩的,CX的本质就是高斯消元
		int i_q_row = 0;
		//eliminate the X matrix into a lower-trangular matrix
		//
		Matrixb::iterator itz=vz.begin();
		Matrixb::iterator itx=vx.begin();
		vector<bool>::iterator itr=r.begin();
		for(int i_q_row=0;i_q_row<N;i_q_row++){
			if(!vx[i_q_row][i_q_row]){
				//if the diagonal term is zero, we find the first 1 below it and exchange these two rows
				int i_q_below=i_q_row+1;
				while(vx[i_q_below][i_q_row]!=1){
					i_q_below++;
				}
				//exchange with three CNOT
				row_exchange(i_q_below,i_q_row);
				exchange_info.push_back(pair<int,int>(i_q_below,i_q_row));
			}
		}
		i_q_row = 0;
		for(typename Matrixb::iterator itx=vx.begin(); itx!=vx.end(); itx++){
			for(int i_q = i_q_row+1; i_q < N; i_q++){
				//in the i_q_row-th row, if i_q is 1, apply the 0-th col to this col and eliminate this 1
				if( (*itx)[i_q] ){
					CX(N-1-i_q_row, N-1-i_q);
				}
			}
			i_q_row++;
		}
		
		//eliminate the X matrix into diagonal
		i_q_row = 0;
		for(typename Matrixb::iterator itx=vx.begin(); itx!=vx.end(); itx++){
			for(int i_q = 0; i_q < i_q_row; i_q++){
				if( (*itx)[i_q] ){
					CX(N-1-i_q_row, N-1-i_q);
				}
			}
			i_q_row++;
		}
		if(DEBUG){
			displayln_debug_info("step 2:");
			show_tableau();
		}
		
		//step 3:-P- matrix decomposition and phase gate
		Matrixb Mz = LLT_decomposition(vz);
		//displayln_vector_vector(Mz);
		i_q_row = 0;
		for(Matrixb::iterator it = Mz.begin(); it!=Mz.end(); it++){

			//inner product of each line of Mz is resulting diagonal term of MM^T
			bool result = bin_inner_prod(*it);
			//apply to add a diagonal A on original Z to derive MM^T: Z+A=MM^T
			if(result!=vz[i_q_row][i_q_row])
				P(N-1-i_q_row);

			i_q_row++;
		}
		if(DEBUG){
			displayln_debug_info("step 3:");
			show_tableau();
		}

		//step 4:-CX- CNOT to creat M on X matrix
		i_q_row = 0;
		for(Matrixb::iterator it = Mz.begin(); it!=Mz.end(); it++){
			vector<bool>::iterator it_end = (*it).end();
			it_end--;
			int i_q_col=0;
			for(vector<bool>::iterator it2 = (*it).begin(); it2!= (*it).end(); it2++){

				//if it2 to point the lower trangular term
				if(it2!=it_end && *it2) 

					CX(N-1-i_q_row, N-1-i_q_col);

				i_q_col++;
			}
			i_q_row++;
		}
		if(DEBUG){
			displayln_debug_info("step 4:");
			show_tableau();
		}

		//step 5:-P- taking Z matrix to zero then tune the symbol to zero
		i_q_row = 0;
		while(i_q_row < N){
			P(i_q_row);
			i_q_row++;
		}	
		//
		//
		if(DEBUG){
			displayln_debug_info("step 5-1:");
			show_tableau();
		}
		i_q_row = 0;
		vector<bool> selection_a;

		itx=vx.begin();
		itr=r.begin();
		while(itx!=vx.end()){
			int i_q_col = 0;
			bool tmp = *itr;
			for(vector<bool>::iterator it2 = (*itx).begin(); it2!= (*itx).end(); it2++){
				if(i_q_col==i_q_row)
					break;

				tmp ^= (*it2 && selection_a[i_q_col]);  //solve the linear equation
				i_q_col++;
			}
			selection_a.push_back(tmp); //collect the solution
			i_q_row++,itx++,itr++;
		}

		i_q_row=0;
		for(vector<bool>::iterator it=selection_a.begin(); it!=selection_a.end(); it++){
			//a_i 为1，则施加两个Phase gate使符号改变
			if(*it){
				P(N-1-i_q_row);
				P(N-1-i_q_row);
			}
			i_q_row++;
		}	

		if(DEBUG){
			displayln_debug_info("step 5:");
			show_tableau();
		}



		//step 6:-CX- use CNOT to perform Gaussian elimination, identical to the second step of step 2.
		for(int i_q_row=0;i_q_row<N;i_q_row++){
			if(!vx[i_q_row][i_q_row]){
				//if the diagonal term is zero, we find the first 1 in the right of it and exchange these two column
				int i_q_below=i_q_row+1;
				while(vx[i_q_below][i_q_row]!=1){
					i_q_below++;
				}
				//exchange with three CNOT
				row_exchange(i_q_below,i_q_row);
				exchange_info.push_back(pair<int,int>(i_q_below,i_q_row));
			}
		}
		i_q_row = 0;
		for(typename Matrixb::iterator itx=vx.begin(); itx!=vx.end(); itx++){
			for(int i_q = 0; i_q < i_q_row; i_q++){
				if( (*itx)[i_q] ){
					CX(N-1-i_q_row, N-1-i_q);
				}
			}
			i_q_row++;
		}
		if(DEBUG){
			displayln_debug_info("step 6:");
			show_tableau();
		}
		
		//step 7:-H-  switch the X identity to Z matrix
		for(int i_q=0; i_q<N; i_q++)
			H(i_q);
		if(DEBUG){
			displayln_debug_info("step 7:");
			show_tableau();
		}

		//构造此stabilizer tableau的clifford circuit 是找到的循序之逆
		reverse(logic_gate_list.begin(),logic_gate_list.end());

		//recover the stabilizer tableau
		vx = origin_vx; //N*N binary matrix for X
		vz = origin_vz; //N*N binary matrix for Z
		r = origin_r;
	} 

	inline Matrixb LLT_decomposition(Matrixb vz){
		Matrixb Mz;
		vector<bool> tmp;
		for(int i=0; i < N; i++){
			tmp.clear();
			for(int j=0; j < i; j++){
				bool M_ele=vz[i][j];
				for(int k=0; k<j; k++){
					M_ele = M_ele^(tmp[k] && Mz[j][k]);
				}
				tmp.push_back(M_ele);
			}

			tmp.push_back(true); //diagonal term of M matrix
			//for(int j=i+1;j<N;j++)
			//	tmp.push_back(false); //upper triangular term of M matrix
			Mz.push_back(tmp); 
		}
		return Mz;
	}

	inline void reverse_clifford_gate_list(){
		/* taking the Hermitian conjugate of the logic_gate_list
		 */
		vector<pairGateSite> reverse_gate_list_tmp = logic_gate_list;
		reverse(reverse_gate_list_tmp.begin(), reverse_gate_list_tmp.end());

		for(auto it = reverse_gate_list_tmp.begin(); it!=reverse_gate_list_tmp.end(); it++){
			if(!(it->first).compare("P")){
				reverse_gate_list.push_back(*it);//add 3 identical Phase gates
				reverse_gate_list.push_back(*it);//add 3 identical Phase gates
				reverse_gate_list.push_back(*it);//add 3 identical Phase gates
			}else{
				reverse_gate_list.push_back(*it);
			}

		}
		
	
	}

	inline void row_exchange(int r1, int r2){
		vector<bool> tmp_x=vx[r1];
		vector<bool> tmp_z=vz[r1];
		bool tmp_r=r[r1];
		vx[r1]=vx[r2];
		vz[r1]=vz[r2];
		r[r1]=r[r2];

		vx[r2]=tmp_x;
		vz[r2]=tmp_z;
		r[r2]=tmp_r;

	}

	inline vector<pairGateSite> bool_vector_to_Pauli_gate_list(vector<bool> vec){
		/* converting the list of bool vector to a gate list, this gate list is implementable on quantum_circuit object
		 */
		vector<pairGateSite> gate_list;
		for(int i_q=0; i_q<N; i_q++){
			int site = N-1-i_q;
			if(vec[i_q]==false and vec[i_q+N]==false)
				gate_list.push_back(pair<string,vector<int> >("Id",{site}));
			else if(vec[i_q]==true and vec[i_q+N]==false)
				gate_list.push_back(pair<string,vector<int> >("X",{site}));
			else if(vec[i_q]==false and vec[i_q+N]==true)
				gate_list.push_back(pair<string,vector<int> >("Z",{site}));
			else if(vec[i_q]==true and vec[i_q+N]==true)
				gate_list.push_back(pair<string,vector<int> >("Y",{site}));
			else
				assert(false);
		}
		return gate_list;
	}

	
	inline bool compare_xz(stabilizer_tableau& st2){  //对传入相同类作为成员函数参数，只能传引用，而不能传一个副本本
		/* compare xz tableau of the st2 object with the current object.
		 * This is used for creating an invariant Entangler
		 * see search_entangler.cpp
		 * Return:
		 * 	0 if vx vz Matrixb are all indentical  "means no difference"
		 * 	1 if not." means have difference "
		 */
		 vector<vector<bool> >::iterator it_1 = vx.begin();
		 vector<vector<bool> >::iterator it_2 = (st2.vx).begin();
		 while(it_1!=vx.end()){
			if(compare_vectorb(*it_1, *it_2))
				return true;

			it_1++;
			it_2++;
		 }

		 it_1 = vz.begin();
		 it_2 = st2.vz.begin();
		 while(it_1!=vz.end()){
			if(compare_vectorb(*it_1, *it_2))
				return true;

			it_1++;
			it_2++;
		 }
		 return false;
	}

	inline void gate_by_pairGateSite(pairGateSite p_gs){
		/*通过pairGateSite施加量子门,使用在destabilizer的构造中
		 */
		string gate_name = p_gs.first;

		if(gate_name=="CX")
			CX(p_gs.second);
		else if(gate_name=="P")
			P(p_gs.second);
		else if(gate_name=="H")
			H(p_gs.second);
		else{
			displayln_error_info(ssprintf("%s gate are not supported by function gate_by_pairGateSite",gate_name.c_str()));
			assert(false);
		}
	}






	////////////////////////////////////////////////////////////////
	////////////////////// clifford simplification //////////////////////////
	inline void gate_list_simplify(){
		gate_list_simplify_Phase(&logic_gate_list);
		gate_list_simplify_Hadamard(&logic_gate_list);
	}
	inline void gate_list_simplify(vector<pairGateSite>* gate_list){
		//simplification sequence
		gate_list_simplify_Phase(gate_list);
		gate_list_simplify_Hadamard(gate_list);
	}

	inline void gate_list_simplify_Hadamard(vector<pairGateSite>* gate_list){
		/* As two hadamard gates are Identity, we identify this bunch of H into NULL gate then erase it
		 *This function is simple adaptation of the next function gate_list_simplify_Phase(). 
		 * */ 
		vector<int> H_gate_num(N,0);//分别代表每个qubit 上积攒的 H gate
		vector<vector<vector<pairGateSite>::iterator > > matrix_it;//record the iterator of logic_gate_list with respect to every qubit
		int site;
		int flag_site= -1;//which qubit should be cleared.
		for(int i=0; i<N; i++){
			vector<vector<pairGateSite>::iterator > vec_it;
			matrix_it.push_back(vec_it);
		}
		for(vector<pairGateSite>::iterator it=gate_list->begin(); it!=gate_list->end(); it++){
			flag_site=-1;
			if( !(it-> first).compare("H")){ //compare前长则返回1，compare前短则返回-1两者在if中都判true
				site = (it->second)[0];
				H_gate_num[site]++;
				matrix_it[site].push_back(it);
				if(H_gate_num[site]==2)//Two Hadamard is Identity
					flag_site = site;
			}else{//if there is other gate interupt the sequence of Hadamard, we clear the bin of recording
				for(vector<int>::iterator it_s=(it->second).begin(); it_s!=(it->second).end(); it_s++){
					matrix_it[(*it_s)].clear();
					H_gate_num[(*it_s)] = 0;
				}
			}
			if(flag_site >= 0){
				for(int i=0; i<2; i++){
					(matrix_it[flag_site][i])->first = "NULL"; //convert the gate name into "NULL" and will be erased
				}
				//clear the recording
				matrix_it[flag_site].clear();
				H_gate_num[flag_site] = 0;
			}
		}
		for(vector<pairGateSite>::iterator it=gate_list->begin(); it!=gate_list->end();){
			if( !(it->first).compare("NULL"))
				it =gate_list->erase(it);
			else
				it++;
			
		}

	}


	inline void gate_list_simplify_Phase(vector<pairGateSite>* gate_list){
		/* As four phase gates are Identity, we identify this bunch of Phase into NULL gate then erase it*/ 
		vector<int> P_gate_num(N,0);//分别代表每个qubit 上积攒的 P gate
		vector<vector<vector<pairGateSite>::iterator > > matrix_it;//record the iterator of logic_gate_list with respect to every qubit
		int site;
		int flag_site= -1;//which qubit should be cleared.
		for(int i=0; i<N; i++){
			vector<vector<pairGateSite>::iterator > vec_it;
			matrix_it.push_back(vec_it);
		}
		for(vector<pairGateSite>::iterator it=gate_list->begin(); it!=gate_list->end(); it++){
			flag_site=-1;
			if( !(it-> first).compare("P")){ //compare前长则返回1，compare前短则返回-1两者在if中都判true
				site = (it->second)[0];
				P_gate_num[site]++;
				matrix_it[site].push_back(it);
				if(P_gate_num[site]==4)//four Phase is Identity
					flag_site = site;
			}else{//if there is other gate interupt the sequence of Phase, we clear the bin of recording
				for(vector<int>::iterator it_s=(it->second).begin(); it_s!=(it->second).end(); it_s++){
					matrix_it[(*it_s)].clear();
					P_gate_num[(*it_s)] = 0;
				}
			}
			if(flag_site >= 0){
				for(int i=0; i<4; i++){
					(matrix_it[flag_site][i])->first = "NULL"; //convert the gate name into "NULL" and will be erased
				}
				//clear the recording
				matrix_it[flag_site].clear();
				P_gate_num[flag_site] = 0;
			}
		}
		for(vector<pairGateSite>::iterator it=gate_list->begin(); it!=gate_list->end();){
			if( !(it->first).compare("NULL"))
				it =gate_list->erase(it);
			else
				it++;
			
		}
	}
	////////////////////////////////////////////////////////////////

	

	////////////////////////////////////////////////////////////////
	////////////////////////获取对象信息函数////////////////////////

	inline void show_tableau(){
		typename Matrixb::iterator it_end = vx.end();
		it_end--;
		typename Matrixb::iterator it_begin = vx.begin();
		
		Matrixb::iterator itx=vx.begin();
		Matrixb::iterator itz=vz.begin();
		vector<bool>::iterator itr=r.begin();
		displayln_message_info("stablizer tableau: X, Z, Sign");
		while(itx!=vx.end()){


			if(itx == it_begin)
				cout<<"/ ";
			else if(itx == it_end)
				cout<<"\\ ";
			else 
				cout<<"| ";

			display_vector((*itx));
			cout<<";";
			display_vector((*itz));
			cout<<"| ";
			cout<<*itr;
			if(itx == it_begin)
				cout<<" \\";
			else if(itx == it_end)
				cout<<" /";
			else 
				cout<<" |";
 			
			cout<<endl;

			itx++,itz++,itr++;
		}
		return;
	}

	inline void show_logic_gate_list(){
		displayln_debug_info("logic_gate_list:");
		displayln_vector_pair_string_vector(logic_gate_list);
		displayln_debug_info("--------------");
	}
	////////////////////////////////////////////////////////////////
	inline vector<vector<bool> > get_full_X(){
		vector<vector<bool> > full_tableau_X;
		for(int i=0; i<d; i++){
			vector<bool> ax(N,0);
			vector<bool> binary = itobinary(i,N);
			for(int j=0;j<N;j++){
				if(binary[j])
					ax=ax^(vx[j]);
			}
			full_tableau_X.push_back(ax);
		}
		return full_tableau_X;
	}
	inline vector<vector<bool> > get_full_Z(){
		vector<vector<bool> > full_tableau_Z;
		for(int i=0; i<d; i++){
			vector<bool> az(N,0);
			vector<bool> binary = itobinary(i,N);
			for(int j=0;j<N;j++){
				if(binary[j])
					az=az^(vz[j]);
			}
			full_tableau_Z.push_back(az);
		}
		return full_tableau_Z;
	}
	inline vector<bool> get_full_r(){
		vector<bool> full_tableau_r;
		for(int i=0; i<d; i++){
			vector<bool> binary = itobinary(i,N);
			int e=0; //phase
			vector<bool> ax(N,0);
			vector<bool> az(N,0);
			for(int j=0;j<N;j++){
				if(binary[j]){
					if(r[j])
						e = e+2;
					for(int k=0;k<N;k++)
						e=e+phase_shift(ax[k],az[k],vx[j][k],vz[j][k]);

					az=az^(vz[j]);
					ax=ax^(vx[j]);

				}
			}

			if(e%4==0)
				full_tableau_r.push_back(false);
			else if(e%2==0)
				full_tableau_r.push_back(true);
			else
				assert(false);

		}
		return full_tableau_r;
			
	}
	inline int phase_shift(bool lx, bool lz, bool rx, bool rz){
		//Pauli term lx;lz * rx;rz 
		//return the phase addition of the result
		int phase=0; //一般情况返回0

		if(( lx &&!lz && rx && rz)||  	//XY=iZ
		   ( lx && lz &&!rx && rz)||	//YZ=iX
		   (!lx && lz && rx &&!rz))		//ZX=iY
				phase=1;
		else if(( lx && lz && rx &&!rz)|| 	//YX=-iZ
			    ( lx &&!lz &&!rx && rz)|| 	//XZ=-iY
				(!lx && lz && rx && rz))	//ZY=-iX	
				phase=3;

		return phase;			
	}

}; //stabilizer_tableau

void read_stabilizer(struct stabilizer_tableau* st, char *fn, char* params){
	/*read a stabilizer generator from file fn, with optional parameters params*/
	displayln_debug_info(ssprintf("read file from %s",fn));

	FILE *fp;
	char fn2[255];
	char c=0;
	int val;

	
	if(params){
		int l = strlen(params);
		for(int i=1; i<l; i++){ //第一个字符为'-'，故从i=1开始
			if((params[i]=='d')||(params[i]=='D')) st->DEBUG = 1;
		}
	}





	sprintf(fn2,"%s", fn);
	fp = fopen(fn2,"r");
	if(!fp){
		sprintf(fn2,"%s.txt",fn); //attach the possible suffix
		fp = fopen(fn2, "r");
		if (!fp) displayln_error_info(ssprintf("file %s not found",fn));
	}
	while (!feof(fp)&&(c!='#'))
		fscanf(fp, "%c",&c);
	if (c!='#') {displayln_error_info("file start symbol \'#\' not found"); assert(false);};
	
	st->N = 0;
	bool flag=false;
	while(!feof(fp))// if the file pointer didn't reach the end
	{
		fscanf(fp, "%c",&c);
		if((c=='\r')||(c=='\n')){
			if(flag==false){
				flag=true;
				continue;
			}
			else break;  //only the first row is scanned
		}
		if((c>=ASCII0+0) && (c<=ASCII0+3))
			st->N++;
	}

	displayln_debug_info(ssprintf("Number of qubits :%d", st->N));
	fp = fopen(fn2,"r");
	while (!feof(fp)&&(c!='#'))
		fscanf(fp, "%c",&c);
	
	vector<bool> x;
	vector<bool> z;
	while(!feof(fp)){

		fscanf(fp, "%c",&c);
		if((c=='\r')||(c=='\n')){
			if(!x.empty()){
				st->vx.push_back(x);
				st->vz.push_back(z);

				x.clear();
				z.clear();
			}
			continue;
		}
		if( c=='+' ) st->r.push_back(false);
		if( c=='-' ) st->r.push_back(true);
		if( c=='0' ) {x.push_back(false); z.push_back(false); }  //I
		if( c=='1' ) {x.push_back(true); z.push_back(false); }  //X
		if( c=='2' ) {x.push_back(true); z.push_back(true); }	//Y
		if( c=='3' ) {x.push_back(false); z.push_back(true); }	//Z

	}
		
	return;

}

inline stabilizer_tableau get_destabilizer_group_generator(stabilizer_tableau* st){
/* calculate the destabilizer_group_generator according to the logic_gate_list
 */
	stabilizer_tableau dest(st->N);

	dest.vx = Matrixb_Id(st->N);
	dest.vz = Matrixb_Zero(st->N);
	dest.r = Vectorb_Zero(st->N);

	displayln_vector_pair_string_vector(st->logic_gate_list);
	for(vector<pairGateSite>::iterator it = st->logic_gate_list.begin(); it!=st->logic_gate_list.end(); it++){
		dest.gate_by_pairGateSite(*it);//logic_gate_list 以及all_gate_list是在门中添加的，不需要手动添加
		
	}



	return dest;
}

}//pquant



