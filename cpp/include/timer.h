#pragma once
#include <sys/time.h>

namespace pquant
{

inline double get_time()
{
	struct timeval tp;
	gettimeofday(&tp, NULL);
	return (double)tp.tv_sec+(double)tp.tv_usec*1e-6;
}

inline double& get_start_time()  //由于是 static double time,故time 这个名字将保存在静态存储区而不被销毁
{
	static double time = get_time();
	return time;
}


inline double get_total_time(){
	return get_time()-get_start_time();
}


}
