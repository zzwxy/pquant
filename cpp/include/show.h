#pragma once 
#include "pquant.h"

#include <cstdio>
#include <string>
#include <iostream>
#include <vector>
#include <string.h>
using namespace std;

typedef pair<string,vector<int> > pairGateSite;

namespace pquant
{


inline std::string vssprintf(const char* fmt, va_list args)
{
  std::string str;
  char* cstr;
  int ret = vasprintf(&cstr, fmt, args);
  assert(ret >= 0);
  str += std::string(cstr);
  std::free(cstr);
  return str;
}

inline std::string ssprintf(const char* fmt, ...)
{
  va_list args;
  va_start(args, fmt);
  return vssprintf(fmt, args);
}



inline FILE*& get_output_file()
{
  static FILE* out = stdout;
  return out;
}

inline FILE*& get_monitor_file()
{
  static FILE* out = NULL;
  return out;
}

inline uint64_t& get_output_level()
{
  static uint64_t x = UINT64_MAX;
  return x;
}

inline void display(const std::string& str, FILE* fp = NULL)
{
  if (NULL == fp) {
    if (get_output_level() == 0) {
      return;
    }
    fp = get_monitor_file();
    if (NULL != fp) {
      fprintf(fp, "%s", str.c_str());
    }
    fp = get_output_file();
  }
  if (NULL != fp) {
    fprintf(fp, "%s", str.c_str());
  }
}


inline void displayln(const std::string& str, FILE* fp = NULL)
{
  if (NULL == fp) {
    if (get_output_level() == 0) {
      return;
    }
    fp = get_monitor_file();
    if (NULL != fp) {
	//控制台输出
      fprintf(fp, "%s\n", str.c_str());
    }
    fp = get_output_file();
  }
  if (NULL != fp) {
    fprintf(fp, "%s\n", str.c_str());
  }
}


inline void displayln_error_info(const std::string& str, FILE* fp = NULL)
{
  if (NULL == fp) {
    if (get_output_level() == 0) {
      return;
    }
    fp = get_monitor_file();
    if (NULL != fp) {
	//监视器输出
      fprintf(fp, "\033[101m--ERROR-- %s\033[0m\n", str.c_str());
    }
    fp = get_output_file();
  }
  if (NULL != fp) {
	  //控制台输出
      fprintf(fp, "\033[101m--ERROR-- %s\033[0m\n", str.c_str());
  }
}
inline void displayln_debug_info(const std::string& str, FILE* fp = NULL)
{
  if (NULL == fp) {
    if (get_output_level() == 0) {
      return;
    }
    fp = get_monitor_file();
    if (NULL != fp) {
	//监视器输出
      fprintf(fp, "\033[91m--DEBUG-- %s\033[0m\n", str.c_str());
    }
    fp = get_output_file();
  }
  if (NULL != fp) {
	  //控制台输出
      fprintf(fp, "\033[91m--DEBUG-- %s\033[0m\n", str.c_str());
  }
}
inline void displayln_message_info(const std::string& str, FILE* fp = NULL)
{
  if (NULL == fp) {
    if (get_output_level() == 0) {
      return;
    }
    fp = get_monitor_file();
    if (NULL != fp) {
	//控制台输出
      fprintf(fp, "\033[96m--MESSAGE-- %s\033[0m\n", str.c_str());
    }
    fp = get_output_file();
  }
  if (NULL != fp) {
      fprintf(fp, "\033[96m--MESSAGE-- %s\033[0m\n", str.c_str());
  }
}



template<class T>
inline void displayln_vector(vector<T> a){
	std::cout<<'{';
	for(typename vector<T>::iterator it=a.begin(); it != a.end(); it++){
		std::cout<<*it<<", ";
	}
	std::cout<<'}'<<std::endl;
}
template<class T>
inline void display_vector(vector<T> a){
	std::cout<<'{';
	typename vector<T>::iterator it_end=a.end();
	it_end--;

	for(typename vector<T>::iterator it=a.begin(); it != a.end(); it++){
		std::cout<<*it;
		if(it!=it_end)
			cout<<',';
	}
	std::cout<<"}";
}
template<class T>
inline void displayln_vector_vector(vector<vector<T> > a){
	for(typename vector<vector<T> >::iterator it = a.begin(); it != a.end(); it++){
		display_vector(*it);
		cout<<endl;
	}
}


template<class K, class T>
inline void displayln_vector_pair_vector(vector<pair<vector<K>, vector<T> > > a){
	for(typename vector<pair<vector<K>, vector<T> > >::iterator it=a.begin(); it != a.end(); it++){
		std::cout<<'(';
		display_vector(it->first)<<',';
		display_vector(it->second);
		std::cout<<')'<<std::endl;
	}
}
template<class T>
inline void displayln_vector_pair_string_vector(vector<pair<string, vector<T> > > a){
	for(typename vector<pair<string, vector<T> > >::iterator it=a.begin(); it != a.end(); it++){
		std::cout<<'(';
		cout<<it->first<<',';
		display_vector(it->second);
		std::cout<<')'<<std::endl;
	}
}

template<class T>
inline void displayln_vector_pair(vector<pair<T, T> > a){
	for(typename vector<pair<T, T > >::iterator it=a.begin(); it != a.end(); it++){
		std::cout<<'(';
		cout<<it->first<<',';
		cout<<it->second;
		std::cout<<')'<<std::endl;
	}
}
inline void displayln_vector_pair_string_string(vector<pair<string, string > > a){
	for(typename vector<pair<string, string > >::iterator it=a.begin(); it != a.end(); it++){
		std::cout<<'(';
		cout<<it->first<<',';
		cout<<it->second;
		std::cout<<')'<<std::endl;
	}
}
inline void display_gate_site(pairGateSite pair_gs){
	cout<<'<'<<pair_gs.first<<',';
	display_vector(pair_gs.second);
	cout<<'>';
}

template<class T,class K>
inline void display_map(map<T,K> Map){
	cout<<'{';
	typename map<T,K>::iterator it_end = Map.end();
	it_end--;
	for(typename map<T,K>::iterator it=Map.begin(); it!=Map.end(); it++){
		cout<<'('<<it->first<<','<<it->second<<')';
		if(it!=it_end)
			cout<<',';
	}
	cout<<'}'<<endl;

}
template<class T,class K>
inline void display_map_vector_ind(map<vector<T>,K> Map){
	cout<<'{';
	typename map<vector<T>,K>::iterator it_end = Map.end();
	it_end--;
	for(typename map<vector<T>,K>::iterator it=Map.begin(); it!=Map.end(); it++){
		cout<<'(';
		display_vector(it->first);
		cout<<','<<it->second<<')';
		if(it!=it_end)
			cout<<',';
	}
	cout<<'}'<<endl;

}

}
