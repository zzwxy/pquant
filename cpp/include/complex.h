#pragma once
#include <complex>

namespace pquant
{
typedef std::complex<double> Complex;

typedef std::complex<float> ComplexF;

template <class T>
std::complex<T> qconj(const std::complex<T>& x)
{
  return std::conj(x);
}

template <class T>
double qnorm(const std::complex<T>& x)
{
  return std::norm(x);
}

const Complex ii(0, 1);


}

