#pragma once 
#include "complex.h"
#include "matrix.h"
#include "timer.h"
#include "pqutil.h"
#include "show.h"
#include "random.h"

#include "Eigen/Dense"
#include <cmath>
#include <iostream>
#include <vector>
#include <algorithm>
#include <string>



using namespace pquant;
using namespace Eigen;
using namespace std;

typedef vector<pair<pairGateSite,float> > vectorNoiseInfo; //<<"gate name",{site}>,probability>
typedef map<pairGateSite, vectorNoiseInfo> NoiseModelMap;

namespace pquant
{
struct noise_model{
	NoiseModelMap noise_model_map;

	int N;//Number of qubits
	vector<float> vec_p0to1,vec_p1to0;

	//initialize with number of qubit
	noise_model(int N_):N(N_){
		for(int i=0;i<N; i++){
			vec_p0to1.push_back(0.0);
			vec_p1to0.push_back(0.0);
		}
	}

	////////////////////// add quantum noise model/////////////////////////
	void add_quantum_noise(string class_name, vector<int> site, vectorNoiseInfo noise_info){

		pairGateSite pair_gate_site(class_name, site);
		
		NoiseModelMap::iterator itr=noise_model_map.find(pair_gate_site);
		if(itr!=noise_model_map.end()) //if found
			itr->second.insert(itr->second.end(),noise_info.begin(), noise_info.end());
		else //if not found, we construct this map
			noise_model_map[pair_gate_site] = noise_info;


	}
	void add_all_qubits_quantum_noise(string class_name, vectorNoiseInfo noise_info){
		/*class name :所有qubit上的哪类门有噪声simple_noise_info
		 *only single qubit incoherent noise can be added with this function
	     *如果噪声信息矢量对象noise_info标定了施加在哪个qubit上，则噪声就添加在这个qubit上
		 * 用法见make_NoiseModelMap.cpp*/
		for(int i_q=0; i_q<N; i_q++){
			add_quantum_noise(class_name, {i_q}, noise_info);
		}
		
	}
	void add_all_qubits_quantum_noise(string class_name, vector<pair<string,float> > simple_noise_info){
		/*class name :所有qubit上的哪类门有噪声simple_noise_info
		 *only single qubit incoherent noise can be added with this function
		 * 如果噪声信息矢量对象simple_noise_info没有标定了施加在哪个qubit上，则噪声被默认为施加在所有qubit上
		 */
		for(int i_q=0; i_q<N; i_q++){
			for(auto it=simple_noise_info.begin(); it!=simple_noise_info.end(); it++){
				vectorNoiseInfo noise_info{pair<pairGateSite,float>(pair<string,vector<int> >(it->first,{i_q}),it->second)};
				add_quantum_noise(class_name, {i_q}, noise_info);

			}
		}
		
	}

	void add_all_qubits_Pauli_noise(string class_name,string Pauli,float prob){
		/* add all qubits after gate "class_name" a Pauli operator "X"/"Y"/"Z" with probability prob
		 * e.g.
		 * add_all_qubits_Pauli_noise("mock","X",0.005);
		 */
		
		vector<pair<string,float> > noise_info{pair<string,float>(Pauli,prob), pair<string,float>("Id",1-prob)};
		add_all_qubits_quantum_noise(class_name,noise_info);
	}


	///////////////////////////////////////////////////////////////////////////

	////////////////////// add measurement noise model/////////////////////////
	void add_measurement_noise(vector<int> site, vector<float> p0to1, vector<float> p1to0){
		/* add measurement noise on qubit of site
		 * site={0,1}
		 * p0to1={0.2,0.2}
		 * p1to0={0.25,0.25}
		 * 用法见make_NoiseModelMap.cpp
		 */
		assert(site.size()==p0to1.size());
		assert(site.size()==p1to0.size());

		for(int i=0; i<site.size(); i++){
			pairGateSite pair_gate_site = {"measure",{site[i]}};	
			vectorNoiseInfo noise_info{pair<pairGateSite,float>(pair<string,vector<int> >("0to1",{site[i]}),p0to1[i]),pair<pairGateSite,float>(pair<string,vector<int> >("1to0",{site[i]}),p1to0[i])};//Creat {<<"0to1",{0}>,0.2>,<<"1to0",{0}>,0.25>}
			noise_model_map[pair_gate_site]= noise_info;

			vec_p0to1[N-site[i]-1]=p0to1[i];
			vec_p1to0[N-site[i]-1]=p1to0[i];

		}
	}
	void add_measurement_noise(vector<float> p0to1, vector<float> p1to0){
		/* add measurement noise on qubit of site{N-1,N-2,...2,1,0}
		 * p0to1={0.2(on qubit 1),0.2(on qubit 0)}
		 * p1to0={0.25(on qubit 1),0.25(on qubit 0)}
		 * 用法见make_NoiseModelMap.cpp
		 */
		assert(p1to0.size()==N);
		assert(p0to1.size()==N);
		

		vector<int> site;
		for(int i=N-1; i>-1; i--){
			site.push_back(i);
		}
		add_measurement_noise(site,p0to1,p1to0);
	}
	void add_all_qubits_measurement_noise(float p0to1, float p1to0){
		/* add measurement noise on qubit of site{N-1,N-2,...2,1,0} with the same bit-flip probability from 0to1 and 1to0
		 * 用法见make_NoiseModelMap.cpp
		 */
		
		vector<float> vec_p0to1(N,p0to1);
		vector<float> vec_p1to0(N,p1to0);

		add_measurement_noise(vec_p0to1,vec_p1to0);
	}
	///////////////////////////////////////////////////////////////////////////
	//
	void clear_noise_model(){
		//清除此对象中的所有噪声模型信息
		noise_model_map.erase(noise_model_map.begin(), noise_model_map.end());
		displayln_debug_info("Noise Model Cleared!");
	}


///////////////////////////////////////////////////////////////
/////////////////    display function      ///////////////////
///////////////////////////////////////////////////////////////
	inline void display_noise_model_map(bool carrage = false){
		//if carrage==ture, it will have carrage between noise model on each qubit
		displayln_message_info("( <class of physical gate, site> ,{<<noise gate, on which site>, probability>,<<,>,>.....} ).....");
		NoiseModelMap::iterator it_nmm_end = noise_model_map.end();
		it_nmm_end--;
		for(NoiseModelMap::iterator it_nmm = noise_model_map.begin(); it_nmm != noise_model_map.end(); it_nmm++){
			cout<<"( ";
			display_gate_site(it_nmm->first);
			cout<<",{";
			vectorNoiseInfo::iterator it_ni_end = it_nmm->second.end();
			it_ni_end--;
			for(vectorNoiseInfo::iterator it_ni = it_nmm->second.begin(); it_ni != it_nmm->second.end(); it_ni++){
				cout<<'<';
				display_gate_site(it_ni->first);
				cout<<','<<it_ni->second<<'>';
				if(it_ni!=it_ni_end)
					cout<<',';
			}
			cout<<"} )";
			if(it_nmm!=it_nmm_end && carrage)
				cout<<",\n";
			else if(it_nmm!=it_nmm_end)
				cout<<",";
		}
		cout<<endl;
	}

};




}
