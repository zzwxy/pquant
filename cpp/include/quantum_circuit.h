#pragma once 

#include "complex.h"
#include "matrix.h"
#include "timer.h"
#include "pqutil.h"
#include "show.h"
#include "random.h"
#include "noise_model.h"
#include "binary.h"

#include "Eigen/Dense"
#include <cmath>
#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
#include <map>
using namespace pquant;
using namespace Eigen;
using namespace std;
typedef Matrix<Complex,Dynamic,1> sVector;   //typedef后面是待定义，#define前面是待定义 真TM变态
typedef Matrix<Complex,Dynamic,Dynamic> sDensity;
typedef pair<vector<int>,vector<bool> > pairIB;
typedef pair<string,vector<int> > pairGateSite;

typedef vector<pair<pairGateSite,float> > vectorNoiseInfo; //<<"gate name",{site}>,probability>
typedef map<pairGateSite, vectorNoiseInfo> NoiseModelMap;


namespace pquant
{
///////////////////////////////////////////////////////////////
/////////////////    auxilliary function    ///////////////////
///////////////////////////////////////////////////////////////


inline int ftoi(float f){
	/*convert a float(or double) to interger
	 */
	return (int)(f+0.01);
}
template<class T>
inline Matrix<T,Dynamic,1> normalize(Matrix<T,Dynamic,1> vector){
	/*normalize the state vector*/
	double normalizer;
	normalizer =std::sqrt(vector.squaredNorm());
	return vector/normalizer;
}
inline map<string,string> map_convertor(map<string, vector<string> > gate_name_map){
	/*converting a class-set vector to a set element-class vector
	 * eg:{(u1,{Z, RZ, },),(u2,{X, Y, RX, RY, },),(cx,{CX, },)}
	 * 		to
	 * 	{(Z,u1),(RZ,u1),(X,u2),(Y,u2),(RX,u2),(RY,u2),(CX,cx)}
	 */
	map<string,string> converted_gate_name_map;
	for(map<string, vector<string> >::iterator it=gate_name_map.begin(); it!=gate_name_map.end(); it++){
		vector<string> name_set = it->second;
		for(vector<string>::iterator it_set=name_set.begin(); it_set!=name_set.end(); it_set++){
			converted_gate_name_map.insert(pair<string,string>(*it_set,it->first));
		}
	}
	return converted_gate_name_map;
}
///////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////
/////////////////define some useful matrices///////////////////
///////////////////////////////////////////////////////////////
inline OneqGate Rx(double theta){
	OneqGate rx;
	rx<<cos(theta/2.0),-(ii)*sin(theta/2.0),
		-(ii)*sin(theta/2.0),cos(theta/2.0);
	return rx;
}
inline OneqGate Ry(double theta){
	OneqGate ry;
	ry<<cos(theta/2.0),-sin(theta/2.0),
		sin(theta/2.0),cos(theta/2.0);
	return ry;
}
inline OneqGate Rz(double theta){
	OneqGate rz;
	rz<<cos(theta/2.0)-(ii)*sin(theta/2.0),0,
		0,cos(theta/2.0)+(ii)*sin(theta/2.0);
	return rz;
}
///////////////////////////////////////////////////////////////
//
//
//
//
//
//
struct quantum_circuit{
	/////////////////////////////////////////////////////////
	/////////////////////////初始变量定义  //////////////////
private:
	//定义门的种类中包含的门集。本程序的量子噪声是根据门的种类来定义的。qiskit也是这么搞的，尽管这样也许比较随性
	vector<string> u1_set{"Z","RZ","P"};
	vector<string> u2_set{"X","Y","RX","RY","H"};
	vector<string> cx_set{"CX"};
	vector<string> mock_set{"Channel"};

public:
	const int N;//number of qubits
	long long d = ftoi(pow(2,N));
	bool NOISY=true;
	
	sVector state_vector;
	vector<pairGateSite> all_gate_list;
	vector<pairGateSite> logic_gate_list;


	const OneqGate& Sx = const_g::get_Sx();
	const OneqGate& Sy = const_g::get_Sy();
	const OneqGate& Sz = const_g::get_Sz();
	const OneqGate& Phase = const_g::get_Phase();
	const OneqGate& Hadamard = const_g::get_Hadamard();
	const TwoqGate& CNOT = const_g::get_CNOT();	

	map<string, vector<string> > gate_name_map={{"u1",u1_set},{"u2",u2_set},{"cx",cx_set},{"mock",mock_set}};

	map<string, string> converted_gate_name_map=map_convertor(gate_name_map);

	/////////////////////////////////////////////////////////
	//////////////////// 量子噪声变量    ////////////////////
	noise_model NM=NULL;

	/////////////////////////////////////////////////////////
	//
	//变量定义结束
	//
	/////////////////////////////////////////////////////////
	
	
	/////////////////////////////////////////////////////////
	//////////////////////       对象初始化      ////////////

	quantum_circuit():N(1){
		state_vector =  sVector::Zero(d);
		state_vector[0] = 1.0;
		init();

	}
	quantum_circuit(const int Nqubit):N(Nqubit){
		state_vector =  sVector::Zero(d);
		state_vector[0] = 1.0;
		init();
	}
	quantum_circuit(const int Nqubit, sVector init_vector):N(Nqubit){
		state_vector = init_vector;
		init();

	}

	void init(){
		state_vector = normalize(state_vector);
		//std::cout<<"normalized initial state vector = "<<std::endl<<state_vector<<std::endl;
	}
	////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////
	//关于量子噪声
	inline void implement_noise(noise_model nm){
		NM=nm;

	}

	inline void quantum_noise(string gate_name, vector<int> site_v){
		/*logic 量子门的噪声添加
		 * gate_name 量子门的名字，根据名字得知添加那种量子门
		 * site_v 量子门添加在哪位qubit上
		 */

		logic_gate_list.push_back(pairGateSite(gate_name,site_v));
		all_gate_list.push_back(pairGateSite(gate_name,site_v));

		if(NOISY == false) return;//没有噪声，直接返回
		else if(NM.noise_model_map.empty()==true)//有噪声，但没定义noise,则报错
		{
			displayln_error_info("noise model is empty");
			assert(false);
		}

		//convert the name of gate into the class name, the noise model implemented according to the gate class name
		//找到gate_name 对应的门类"u1","u2","cx","mock",来决定施加何种噪声
		pairGateSite p_gs(converted_gate_name_map[gate_name],site_v);
		
		NoiseModelMap::iterator it_nmm;
		it_nmm = NM.noise_model_map.find(p_gs);
		//这个找p_gs。。。显然如果有"cx"，但是 site_v的顺序不对，也是找不到的。所以如果要添加cx量子门的噪声的话，对{0,1},{1,0}这两种情况是独立的
		if(it_nmm != NM.noise_model_map.end()){  //如果找到了
			gate_set_with_probability(it_nmm->second);

		} 
	}


	/* example of noise_info object
	{<<Id,{0}>,0.8>,<<X,{0}>,0.2>}*/
	inline void gate_set_with_probability(vectorNoiseInfo noise_info){
		/*以一定的概率分布函数施加量子门
		 * noise_info 量子噪声信息矢量，其第二个参量构成的矢量是概率密度函数，需要通过求和转化为概率分布函数
		 * 如果概率分布函数之和不为1，则剩下的概率不进行任何操作，相当于Id门
		 */
		float distribution=0.0;

		uniform_real_distribution<float> dis_float(0,1);

		float epsilon = dis_float(RANDOM);

		auto it = noise_info.begin();

		//想象构造这样一个概率分布函数{0.8,1.0}
		for(; it!=noise_info.end(); it++){
			distribution += it->second;
			if(epsilon < distribution)
				break;
		}
		pairGateSite noise_p_gs = it->first; //被概率选中的要施加的噪声门
		gate_by_pairGateSite(noise_p_gs, false);//用pairGateSite对象施加一个量子门,量子噪声门中的门肯定是无噪声的，不然就无限套娃了
	}

	inline vector<bool> measurement_noise(vector<bool> bin_str){
	// add measurement noise(bit-flip noise) according to the noise_model_map of NM
	// flip each bit in "bin_str" with the probabilities recorded
	
	// the flip probability of bin_str, it is extracted from 
	vector<float> flip_probability;
	vector<bool> bin_flip;
	for(int i=0; i<N; i++){
		if(bin_str[i])//If the site is 1, the flip probability record the data from vector of flip from 1 to 0
			flip_probability.push_back(NM.vec_p1to0[i]);
		else
			flip_probability.push_back(NM.vec_p0to1[i]);
	}
	bin_flip=random_sample01_from_1prob_vec(flip_probability);//function defined in random.h
	bin_str = bin_flip^bin_str; //XOR to deterine whether the bit should be fliped

	return bin_str;
	
	}
	////////////////////////////////////////////////////////////////
	
	////////////////////////////////////////////////////////////////////
	///////////////////////功能函数//////////////////////////////////////
	inline void gate_by_pairGateSite(pairGateSite p_gs, bool if_noisy=true){
		/*通过pairGateSite施加量子门,使用在量子噪声门的施加中
		 */
		// p_gs=<"XXI",{2,1,0}>: add X gate on 2-th qubit,
		// 						add X gate on 1-th qubit,
		// 						add Id gate on 0-th qubit.
		string gate_string_name = p_gs.first;

		if(gate_string_name=="Id")
			Id(p_gs.second,if_noisy);
		else if(gate_string_name=="CX")
			CX(p_gs.second,if_noisy);
		else{
			int len = gate_string_name.size();
			for(int i=0; i<len; i++ ){
				char gate_name=gate_string_name[i];
				vector<int> site_v =  {p_gs.second[i]};

				if(gate_name=='X')
					X(site_v, if_noisy);
				else if(gate_name=='Y')
					Y(site_v, if_noisy);
				else if(gate_name=='Z')
					Z(site_v, if_noisy);
				else if(gate_name=='P')
					P(site_v, if_noisy);
				else if(gate_name=='H')
					H(site_v, if_noisy);
				else if(gate_name=='I')
					Id(site_v,if_noisy);
				else{
					displayln_error_info(ssprintf("%s gate are not supported by function gate_by_pairGateSite",gate_name));
					assert(false);
				}
			}
		}

	}
	

	inline void append_gate_list(vector<pairGateSite> gate_list, bool if_noisy=true){
		for(vector<pairGateSite>::iterator it = gate_list.begin(); it!=gate_list.end(); it++){
			gate_by_pairGateSite(*it);//logic_gate_list 以及all_gate_list是在门中添加的，不需要手动添加
			
		}
	}

	inline void construct_by_gate_list(vector<pairGateSite> gate_list, bool if_noisy=true){
		/* construct the quantum circuit from gate_list, the initial state is the standard initial state |000>
		 * the default state preparation is noisy
		 */
		sVector sv = sVector::Zero(d);
		sv[0] = 1.0;
		state_reset(sv);
		append_gate_list(gate_list,if_noisy);
	}




	
	inline void state_reset(sVector reset_vector){
		/*将态矢量重置（同时确保归一化）*/
		state_vector = normalize(reset_vector);
		
		//all_gate_list 重置
		all_gate_list.clear();
		//logic_gate_list 重置
		logic_gate_list.clear();
	}

	inline void state_reset(){
		/* state reset to |0000> */

		sVector sv = sVector::Zero(d);
		sv[0]=1.0;
		state_reset(sv);
	}
	
	inline int pair_list_to_site(vector<pairIB> index_list){
		/*位点列表转化为整型
		 *index_list: {<{1,2},{true, false}>,<{0},{false}>,<{3},{true}>}
		 *对应computational basis 下的态矢量为|1010>即返回site值为10
		 *这里|1010>对应的qubit号为{3210},顺序与qiskit相同
		 */
		vector<bool> bool_list;
		int site=0;
		for(int i=0; i<N; i++ ){
			for(vector<pairIB>::iterator it=index_list.begin(); it !=index_list.end(); it++){
				int label;
				label=0;
				vector<int>::iterator first_it=(it->first).begin();
				for(; first_it != (it->first).end(); first_it++,label++){
					if(*first_it==i){
						break;
					}
				}
				if(first_it !=(it->first).end()){//不等则说明已经找到了这个数字i
					bool_list.push_back((it->second)[label]);
					break;
				}
			}
		}
		for(int i=0; i<N; i++){
			if (bool_list[i]==true){
				site+=ftoi(pow(2,i));
			}	
		}
		return site;
	}

	template<class T>
	inline sVector gate(Matrix<Complex,Dynamic,Dynamic> unitary, T site_){
		/*对于非基础类的成员变量（sVector)作为默认成员方法的参数输入是不行的，必须要将函数重新定义一下*/
		return gate(unitary, site_, state_vector);	
		
	}
	template<class T>
	inline sVector gate(Matrix<Complex,Dynamic,Dynamic> unitary, T site_, sVector svector){
		/*量子门的基础函数，运算量为2^N*2^Ng, N为问题规模，Ng为门作用的qubit数
		 *unitary: 量子门的矩阵表示
		 *site_: 量子门所施加的qubit位点
		 *svector: 被作用的量子态，一般是类成员变量 state_vector
		 * */

		const int size = ftoi(log2(unitary.rows()));
		const int res_size = N-size;
	
		
		vector<int> site;
		//	site.swap(site_);

		//site.push_back(site_);
		site.swap(site_);//将site_中的数据转移到site中，site_为空，若要拷贝这段内存，则使用site.assign(site_)
		vector<int> res_site;

		for(int i=0; i<N; i++){
			vector<int>::iterator it = std::find(site.begin(),site.end(), i);//找site中是否有i
			if(it==site.end()){ //如果没有找到，则将此比特位放入 res_size
				res_site.push_back(i);
			}
		
		}
		//binary set of interacting qubits
		vector<vector<bool> > binary_list;

		//binary set of the rest qubits
		vector<vector<bool> >res_binary_list;
		
		const int d_int = ftoi(pow(2,size));
		const int d_unint = ftoi(pow(2,res_size));
		for(int i=0; i<d_int; i++){
			binary_list.push_back(itobinary(i,size));
		}

		sVector result_vector = sVector::Zero(d);

		if (res_size>0.1){
			for(int i=0; i<d_unint; i++){
				res_binary_list.push_back(itobinary(i,res_size));
			}
			for(int res_ind=0; res_ind<d_unint; res_ind++){
				pairIB res_pair = pairIB(res_site,res_binary_list[res_ind]);
				for(int ind=0; ind<d_int; ind++){
					pairIB int_pair = pairIB(site,binary_list[ind]);
					for(int ind_prod=0; ind_prod<d_int; ind_prod++){
						pairIB prod_pair = pairIB(site,binary_list[ind_prod]);
											
						vector<pairIB> result_list{
							res_pair, int_pair
						};
						vector<pairIB> svector_list{
							res_pair, prod_pair
						};
						//displayln_vector_pair_vector(result_list);
						result_vector(pair_list_to_site(result_list)) += unitary(ind,ind_prod)*svector(pair_list_to_site(svector_list));
					}
				}
			}

		}
		else
		{
			for(int ind=0; ind<d_int; ind++){
				pairIB int_pair = pairIB(site,binary_list[ind]);
				for(int ind_prod=0; ind_prod<d_int; ind_prod++){

						pairIB prod_pair = pairIB(site,binary_list[ind_prod]);

						vector<pairIB> result_list{
							int_pair
						};
						vector<pairIB> svector_list{
							prod_pair
						};
					result_vector(pair_list_to_site(result_list)) += unitary(ind,ind_prod)*svector(pair_list_to_site(svector_list));
				}
			}
		}
		return result_vector;

	}//gate function end

	////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////
	///////////////////////定义常量子门/////////////////////////////////

	inline void Id(vector<int> site_v, bool if_noisy=true){
		string gate_name="Id";


		if(if_noisy){  //logic gate is noisy
			quantum_noise(gate_name,site_v);
		}else{
			all_gate_list.push_back(pairGateSite("\033[107m"+gate_name+"\033[0m",site_v));
		}

	}
	inline void Id(int site, bool if_noisy=true){
		vector<int> site_v{site};
		Id(site_v,if_noisy);
	}

	inline void X(vector<int> site_v, bool if_noisy=true){
		string gate_name="X";

		state_vector = gate(Sx, site_v);
		if(if_noisy){  //logic gate is noisy
			quantum_noise(gate_name,site_v);
		}else{
			all_gate_list.push_back(pairGateSite("\033[107m"+gate_name+"\033[0m",site_v));
		}

	}
	inline void X(int site, bool if_noisy=true){
		vector<int> site_v{site};
		X(site_v,if_noisy);
	}

	inline void Y(vector<int> site_v, bool if_noisy=true){
		string gate_name="Y";

		state_vector = gate(Sy, site_v);
		if(if_noisy){  //logic gate is noisy
			quantum_noise(gate_name,site_v);
		}else{
			all_gate_list.push_back(pairGateSite("\033[107m"+gate_name+"\033[0m",site_v));
		}
	}
	inline void Y(int site, bool if_noisy=true){
		vector<int> site_v{site};
		Y(site_v,if_noisy);
	}


	inline void Z(vector<int> site_v, bool if_noisy=true){
		string gate_name="Z";

		state_vector = gate(Sz, site_v);
		if(if_noisy){  //logic gate is noisy
			quantum_noise(gate_name,site_v);
		}else{
			all_gate_list.push_back(pairGateSite("\033[107m"+gate_name+"\033[0m",site_v));
		}
	}
	inline void Z(int site, bool if_noisy=true){
		vector<int> site_v{site};
		Z(site_v,if_noisy);
	}

	inline void P(vector<int> site_v, bool if_noisy=true){
		string gate_name="P";

		state_vector = gate(Phase, site_v);
		if(if_noisy){  //logic gate is noisy
			quantum_noise(gate_name,site_v);
		}else{
			all_gate_list.push_back(pairGateSite("\033[107m"+gate_name+"\033[0m",site_v));
		}
	}
	inline void P(int site, bool if_noisy=true){
		vector<int> site_v{site};
		P(site_v,if_noisy);
	}


	inline void H(vector<int> site_v, bool if_noisy=true){
		string gate_name="H";

		state_vector = gate(Hadamard, site_v);
		if(if_noisy){  //logic gate is noisy
			quantum_noise(gate_name,site_v);
		}else{
			all_gate_list.push_back(pairGateSite("\033[107m"+gate_name+"\033[0m",site_v));
		}
	}
	inline void H(int site, bool if_noisy=true){
		vector<int> site_v{site};
		H(site_v,if_noisy);
	}


	inline void CX(vector<int> site_v, bool if_noisy=true){
		string gate_name="CX";

		state_vector = gate(CNOT, site_v);
		if(if_noisy){  //logic gate is noisy
			quantum_noise(gate_name,site_v);
		}else{
			all_gate_list.push_back(pairGateSite("\033[107m"+gate_name+"\033[0m",site_v));
		}
	}
	//for user :CX(0,1) 0-control,1-target
	//for programmer :CX({1,0}) 0-control,1-target
	inline void CX(int control, int target, bool if_noisy=true){
		if(control>=N)
			displayln_error_info(ssprintf("CNOT control %d is out of bounds with qubits number %d",control,N));
		if(target>=N)
			displayln_error_info(ssprintf("CNOT target %d is out of bounds with qubits number %d",target,N));
		vector<int> site_v{target, control};
		CX(site_v,if_noisy);
	}


	inline void RX(double theta, vector<int> site_v, bool if_noisy=true){
		string gate_name="RX";

		state_vector = gate(Rx(theta),site_v); //OneqGate虽然不完全是Matrix类型，但是继承Matrix,也是可以当做是Matrix类型
		if(if_noisy){  //logic gate is noisy
			quantum_noise(gate_name,site_v);
		}else{
			all_gate_list.push_back(pairGateSite("\033[107m"+gate_name+"\033[0m",site_v));
		}

	}
	inline void RX(double theta, int site, bool if_noisy=true){
		vector<int> site_v{site};
		RX(theta, site_v,if_noisy);
	}


	inline void RY(double theta, vector<int> site_v, bool if_noisy=true){
		string gate_name="RY";

		state_vector = gate(Ry(theta),site_v);
		if(if_noisy){  //logic gate is noisy
			quantum_noise(gate_name,site_v);
		}else{
			all_gate_list.push_back(pairGateSite("\033[107m"+gate_name+"\033[0m",site_v));
		}

	}
	inline void RY(double theta, int site, bool if_noisy=true){
		vector<int> site_v{site};
		RY(theta, site_v,if_noisy);
	}


	inline void RZ(double theta, vector<int> site_v, bool if_noisy=true){
		string gate_name="RZ";

		state_vector = gate(Rz(theta),site_v);
		if(if_noisy){  //logic gate is noisy
			quantum_noise(gate_name,site_v);
		}else{
			all_gate_list.push_back(pairGateSite("\033[107m"+gate_name+"\033[0m",site_v));
		}

	}
	inline void RZ(double theta, int site, bool if_noisy=true){
		vector<int> site_v{site};
		RZ(theta, site_v,if_noisy);
	}
	///////////////////////定义复合量子门/////////////////////////////////
	inline bool* Random_Pauli(){
		//Y_1 X_0 返回 a为 (1,1 , 1,0) 
		//a_x=(1,1)
		//a_z=(1,0)
		bool* a = random_uniform_01(2*N);
		bool* a_x = a;
		bool* a_z = a+N;



		for(int i_q=0; i_q<N; i_q++){
			if(a_x[i_q]==false and a_z[i_q]==false)
				Id(N-1-i_q);//之所以要写这么蹩脚的顺序是为了与stabilizer tableau中的矢量对应
			else if(a_x[i_q]==true and a_z[i_q]==false)
				X(N-1-i_q);
			else if(a_x[i_q]==false and a_z[i_q]==true)
				Z(N-1-i_q);
			else if(a_x[i_q]==true and a_z[i_q]==true)
				Y(N-1-i_q);
			else
				assert(false);
		}
		//delete []a;

		return a;
	}

	// Define a quantum channel of which error rate to be detected.
	inline void Channel( bool if_noisy=true){ 
		string gate_name="Channel";


		if(if_noisy){  //logic gate is noisy
			for(int i_q=0; i_q<N; i_q++){
				vector<int> site_v{i_q};
				quantum_noise(gate_name,site_v);
			}
		}

	}
	

	////////////////////////////////////////////////////////////////
	//
	//
	////////////////////////////////////////////////////////////////
	///////////////////////    measurement     /////////////////////
	//有两种measurement 方案，第一种是通过计算输入算符与当前state vector的overlap直接计算期望值
	//这是一种偷懒的做法
	//还有一种是通过随机数模拟当前量子态的坍缩过程，是真实的量子力学
	inline Complex expect_overlap(Matrix<Complex, Dynamic,Dynamic> op, vector<int> site_v){
		/*return the expectation value <op> with current state vector
		 *site_v is the site of op applied on
		 *if site_v has more than one element, that means op at these site  applied on the state_vector seperately
		 * eg: op = Sz, site_v = {0,1,2}
		 * then <bra|IIZ*IZI*ZII|ket> will be returned.
		 * */
		sVector ket = state_vector; // O|\psi>
		sVector bra = state_vector;

		for(vector<int>::iterator it = site_v.begin(); it!= site_v.end(); it++){

			assert(*it<N);

			vector<int> s_v{*it};
			ket = gate(op, s_v, ket);
		}
		return bra.adjoint()*ket;//如果site_v为空，返回1，没毛病

	}
	
	inline map<vector<bool>,long> measure(long shots, bool if_noisy=true){
		
		return measure(shots, state_vector, if_noisy);
	}
	inline map<vector<bool>,long> measure(long shots, sVector svector, bool if_noisy=true){
		/* measurement function true-|1> false-|0> 
		 * return a map {({00},25),({01},23),({10},26),({11},26)},
		 *           or {({false,false},25),({false,true},23),({true,false},26),({true,true},26)}.
		 * */
		//generator a list for amplitude square
		sVector prob_list = svector.conjugate().array()*svector.array();
		//cout<<prob_list<<endl;

		map<vector<bool>,long> counts;

		vector<double> dist_list;
		vector<bool> i_q_bin;
		double distribution = 0.0;
		float epsilon = 0.0;
		uniform_real_distribution<float> dis_float(0,1);

		//calculate the distribution list of the corresponding probability list
		for(auto it = prob_list.begin(); it!= prob_list.end(); it++){
			distribution+= (*it).real();
			dist_list.push_back(distribution);
		}

		for(long i_shot=0; i_shot<shots; i_shot++){
			//in each loop, make one sampling from the distribution list 
			epsilon = dis_float(RANDOM);

			int i_q = 0;
			for(vector<double>::iterator it = dist_list.begin(); it!=dist_list.end(); it++){
				if(epsilon<(*it))
					break;
				else i_q++;
					
			}

			i_q_bin = itobinary(i_q,N);

			if(NOISY==true && NM.noise_model_map.empty()==false && if_noisy==true)
				i_q_bin = measurement_noise(i_q_bin);

			if(counts.find(i_q_bin) != counts.end())
				counts[i_q_bin]++;
			else
				counts[i_q_bin] = 1;

		}
		//if(NOISY == true)
			
		return counts;
	}

	////////////////////////////////////////////////////////////////
	////////////////////////获取对象信息函数////////////////////////
	
	///////////////////////get function////////////////////////
	inline sDensity get_density_matrix(){
		sDensity density = state_vector*state_vector.adjoint();
		return density;
	}

	static const quantum_circuit& get_instance(){
		static const quantum_circuit& smcs = quantum_circuit();
		return smcs;
	}

	static const map<string,string>& get_converted_gate_map(){
		return get_instance().converted_gate_name_map;
	}
	static const map<string, vector<string> >& get_gate_map(){
		return get_instance().gate_name_map;
	}

	///////////////////////show function////////////////////////
	
	inline void show_state_vector(){
		cout<<"current state vector = "<<endl<<'{'<<state_vector.transpose()<<'}'<<endl;
	}
	inline void show_density_matrix(){
		cout<<"current density matrix = "<<endl<<get_density_matrix()<<endl;
	}
	inline void show_all_gate_list(){
		displayln_debug_info("all_gate_list:");
		displayln("(\033[107m#\033[0m: noise contamination.)");
		displayln_vector_pair_string_vector(all_gate_list);
		if(all_gate_list.size()==0)
			displayln("NULL");
		displayln_debug_info("--------------");
	}
	inline void show_logic_gate_list(){
		displayln_debug_info("logic_gate_list:");
		displayln_vector_pair_string_vector(logic_gate_list);
		if(logic_gate_list.size()==0)
			displayln("NULL");
		displayln_debug_info("--------------");
	}



	////////////////////////////////////////////////////////////////
	

};
	



}//namespace pquant
