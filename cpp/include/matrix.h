#pragma once 
#include "Eigen/Dense"
#include <cmath>
#include "complex.h"
namespace pquant
{


typedef Eigen::Matrix<Complex,2,2,Eigen::RowMajor> OneqGate;
typedef Eigen::Matrix<Complex,4,4,Eigen::RowMajor> TwoqGate;

struct const_g{
	OneqGate Id;
	OneqGate Sx,Sy,Sz;
	OneqGate Phase,Hadamard;
	TwoqGate CNOT;
	
	double inverse_sqrt2;

	inline const_g(){
		inverse_sqrt2 = 1/std::sqrt(2);
		init();
	}
	inline void init(){
		Complex ii(0.0,1.0);
		Id << 1,0,0,1;
		Sx << 0,1,1,0;
		Sy << 0,-ii,ii,0;
		Sz << 1,0,0,-1;
		Phase << 1,0,0,ii;
		Hadamard<< inverse_sqrt2,inverse_sqrt2,inverse_sqrt2,-inverse_sqrt2;
		CNOT  << 1,0,0,0,
					0,0,0,1,
					0,0,1,0,
					0,1,0,0;

	}
	static const const_g& get_instance(){
		static const const_g& smcs = const_g();
		return smcs;
	}
	
	static const OneqGate& get_Id(){
		return get_instance().Id;
	}
	static const OneqGate& get_Sx(){
		return get_instance().Sx;
	}
	static const OneqGate& get_Sy(){
		return get_instance().Sy;
	}
	static const OneqGate& get_Sz(){
		return get_instance().Sz;
	}
	static const OneqGate& get_Phase(){
		return get_instance().Phase;
	}
	static const OneqGate& get_Hadamard(){
		return get_instance().Hadamard;
	}
	static const TwoqGate& get_CNOT(){
		return get_instance().CNOT;
	}

};















}
