#pragma once

#include "pqutil.h"
#include "show.h"

#include <cmath>
#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
#include <map>

using namespace pquant;
using namespace Eigen;
using namespace std;

//This headfile dealing with bool vectors

namespace pquant
{

inline vector<bool> itobinary(int i, int size){
	/*convert an integer to a binary vector
	 * i: integer to be converted
	 * size: length of binary result
	 * Eg: 
	 * Input: itobinary(5,4)
	 * Output:vector<bool> {false,ture,false,true}  (i.e. 0101==5)
	 * as this function is defined as height weight at left, the right end of sequence 0101 represent qubit 0
	 * (follow IBM qiskit convention)
	 */
	vector<bool> binary(size);//默认长度为size的bool形容器全部初始化为false;
	vector<bool>::iterator it=binary.end();
	while(i){
		it--;
		*it = i%2;
		i/=2;
	}
	return binary;

}

inline bool operator<(vector<bool> a, vector<bool> b){
	assert(a.size()==b.size());
	vector<bool>::iterator it_a = a.begin();
	vector<bool>::iterator it_b = b.begin();
	for(;it_a!=a.end();it_a++,it_b++){
		if(*it_a==true && *it_b==false){
			return false;
		}
		else if(*it_a==false && *it_b==true){
			return true;
		}
	}
	return false;//两者相等，返回false
}


inline bool operator>(vector<bool> a, vector<bool> b){
	assert(a.size()==b.size());
	vector<bool>::iterator it_a = a.begin();
	vector<bool>::iterator it_b = b.begin();
	for(;it_a!=a.end();it_a++,it_b++){
		if(*it_a==true && *it_b==false){
			return true;
		}
		else if(*it_a==false && *it_b==true){
			return false;
		}
	}
	return false;//两者相等，返回false
}

}
