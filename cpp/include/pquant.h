#pragma once 
//If the compile option doesn't include -std=c++11
#if __cplusplus < 201103L
       #error "Should use -std=c++11 option for compile
#endif

#include "quantum_circuit.h"
#include "complex.h"
#include "matrix.h"
#include "timer.h"
#include "pqutil.h"
#include "show.h"
#include "random.h"
#include "noise_model.h"
#include "stabilizer_formalism.h"
#include "fitting.h"
#include "binary.h"

#include "Eigen/Dense"
#include <cmath>
#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
#include <map>
#include <fstream>



