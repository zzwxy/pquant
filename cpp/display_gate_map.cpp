
#include "pquant.h"


using namespace pquant;
using namespace Eigen;
using namespace std;


int main(){

	vector<string> u1_set = {"Z","RZ"};
	vector<string> u2_set = {"X","Y","RX","RY"};
	vector<string> cx_set ={"CX"};

////////////////////////////////////////////////////////////////////////
	map<string, vector<string> > gate_name_map={{"u1",u1_set},{"u2",u2_set},{"cx",cx_set}};

	map<string, string> converted_gate_name_map=map_convertor(gate_name_map);

	cout<<"DIY converted_gate_name_map"<<endl;
	display_map(converted_gate_name_map);
	cout<<"quantum_circuit converted_gate_name_map"<<endl;
	display_map(quantum_circuit::get_converted_gate_map());
    //Ideal output: {(CX,cx),(RX,u2),(RY,u2),(RZ,u1),(X,u2),(Y,u2),(Z,u1)}
	return 0;
}
