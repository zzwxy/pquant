
#include "pquant.h"


using namespace pquant;
using namespace Eigen;
using namespace std;

//example of run this program:
//./a.out (-d) stabilizer/stabilizer_X.txt

int main(int argc, char **argv){
	
	struct stabilizer_tableau *st;
	int param =0; //whether there are command-line parameters;
	if(argc==1) {
		displayln_error_info("stabilizer filename should be input");
		return 0;
	}

	if(argv[1][0]=='-') param=1;

	st = (struct stabilizer_tableau *)malloc(sizeof(struct stabilizer_tableau));

	if(param) read_stabilizer(st, argv[2],argv[1]);
	else read_stabilizer(st, argv[1],NULL);

	displayln_debug_info("initial stabilizer tableau");
	st->show_tableau();
	
	st->generating_clifford_list();
	

	st->show_logic_gate_list();

	st->gate_list_simplify();

	st->show_logic_gate_list();
	return 0;
}
