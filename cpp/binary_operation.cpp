
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <iostream>

using namespace std;
int main(){
	long x = 1111;
	long x2;
	cout<<sizeof(x)<<endl;

	//原样输出x的二进制数，由此可见，longlong 型数据是以big endian
	
    cout<<bitset<sizeof(x)*8>(x)<<endl;

	x2 = x>>1;//右移一位bit,左边空出来的一位补零
    cout<<bitset<sizeof(x2)*8>(x2)<<endl;
    cout<<bitset<sizeof(x2)*8>(x2&31)<<endl; //只取右边的5位
	
	return 0;
}
