
#include "pquant.h"


using namespace pquant;
using namespace Eigen;
using namespace std;

int main(){

	//std::default_random_engine random;
	int length = 20;
	bool* bernoulli_array = random_01(0.1,length);//在random.h中定义的宏仍然可以在这里得到
	vector<bool> bernoulli_vector(bernoulli_array, bernoulli_array+length);
	displayln_vector(bernoulli_vector);
	delete []bernoulli_array;//必须要手动将new出来的堆删除



	return 0;
}
