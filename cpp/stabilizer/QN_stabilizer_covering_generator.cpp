
#include "pquant.h"
#include <unistd.h>
using namespace pquant;
using namespace std;

inline vector<bool> Teichmuller_reduce(vector<bool> Teich, vector<bool> primitive, int Nqubit){
	//使用长除法求Teichmuller 元素除以 primitive的余数，余数即为约化后的Teichmuller 元，称为代表元(representitive)
	vector<bool> quotient;
	int shift=0;

	if(Teich.size()<primitive.size())  //作为primitive polynomial，其list的第一个元素一定是true,故只要Teich长度小于primitive长度，传入的Teich 就一定小于primitive了，就一定可以直接返回。
		return Teich;
	//首先，删除左边的0元素
	while(1){
		if(*Teich.begin()==false)
			Teich.erase(Teich.begin());
		else
			break;
	}

	vector<bool> tmp(Teich.begin(),Teich.begin()+Nqubit+1);
	for(vector<bool>::iterator it_r=Teich.begin()+Nqubit+1;it_r!=Teich.end();it_r++){
		if(tmp[0]==1)
			tmp=tmp^primitive; //做一次减法

		//去掉首个元素（应该是0），补上被除数中的后一位的数字
		tmp.erase(tmp.begin()); 
		tmp.push_back(*it_r);
	}
	if(tmp[0]==1)
		tmp=tmp^primitive;

	//删除首元素（应该是false）
	tmp.erase(tmp.begin());
	return tmp;
}
inline string number_to_Pauli(string number){
	int len = number.size();
	string Pauli;
	for(int i=0; i<len; i++){
		switch(number[i]){
		case '0':
			Pauli+="I";
			break;
		case '1':
			Pauli+="X";
			break;
		case '2':
			Pauli+="Y";
			break;
		case '3':
			Pauli+="Z";
			break;
		default:
			displayln("input string error");
		}
	}
	return Pauli;

}
inline void write_stabilizer(vector<string> stabilizer_list,int Nqubit){
	//creat the path folder if not exist
	string path=ssprintf("Q%d_stabilizer_covering",Nqubit);
	
	if (access(path.c_str(),0) !=0 ){
		string sentence = "mkdir "+path;
		system(sentence.c_str());
	}
	ofstream fout;
	fout.open("./"+path+"/"+"stabilizer_"+number_to_Pauli(stabilizer_list[0])+".txt");
	fout<<"This file is created by program QN_stabilizer_covering_generator.cpp ."<<endl;
	fout<<"#"<<endl;
	for(int i=0; i<Nqubit;i++){
		fout<<"+";
		for(int j=0; j<Nqubit; j++){
			fout<<" "<<stabilizer_list[i][j];
		}
		fout<<endl;
	}
	fout.close();
}
inline char Pauli_Product(char p1, char p2){
	if(p1==p2)
		return '0'; //I*I=I, X*X=I, Y*Y=I,Z*Z=I
	else if(p1=='0')
		return p2; //I*X=X, I*Y=Y, I*Z=Z
	else if(p2=='0')
		return p1; //X*I=X, Y*I=Y, Z*I=Z
	else{
		if(p1=='1' && p2=='2') return '3'; //X*Y=Z
		if(p1=='2' && p2=='3') return '1'; //Y*Z=X
		if(p1=='3' && p2=='1') return '2'; //Z*X=Y
		if(p1=='2' && p2=='1') return '3';
		if(p1=='3' && p2=='2') return '1';
		if(p1=='1' && p2=='3') return '2';

	}
	return '0';
}
inline string Pauli_merge(string Pauli1, string Pauli2, int Nqubit){
	for(int i=0; i<Nqubit; i++){
		Pauli1[i]=Pauli_Product(Pauli1[i], Pauli2[i]);
	}	
	return Pauli1;
}



int main(int argc, char **argv){

	int param=0;
	if(argc==1) {
		displayln_error_info("Number of qubits and the primitive polynomial should be input");
		return 0;
	}
	else if(argc==2){
		displayln_error_info("The primitive polynomial should be input");
		return 0;

	}

	int Nqubit = argv[1][0]-48;
	int Nqubit_m1=Nqubit-1;
	if(strlen(argv[2])-1!= Nqubit){
		displayln_error_info("Length of the primitive polynomial and the number of qubit are not matching");
		return 0;

	}

	
	int d = ftoi(pow(2,Nqubit));
	int d_m1 = d-1;
	vector<vector<bool> > Teichmuller;
	//Construct the Teichmuller set {0,X,X^2,X^3}->{{0,0,0,0},{0,0,1,0},{0,1,0,0},{1,0,0,0}} 见Multicomplementary operator via finite Fourier transform Eq(3.2)
	for(int N_ind=1; N_ind<=d; N_ind++){
		vector<bool> tmp(d,0);
		if(N_ind>1)
			tmp[d-N_ind]=1;

		Teichmuller.push_back(tmp);
	}
	//displayln_vector_vector(Teichmuller);
	
	int l_primitive = strlen(argv[2]);  //lenth of primitive polynomial string
	vector<bool> primitive(l_primitive,0); //the bool vector representation of monic primitive polynomial
	for(int i=0; i<l_primitive; i++){
		if(argv[2][i]=='1')
			primitive[i]=true;
	}
	//displayln_vector(primitive);

	vector<vector<bool> > representative_list;
	for(int i=0; i<d; i++){
	 	vector<bool> representative(Nqubit,0);

		//i=0,1,...Nqubit-1时Teichmuller元素本身就是代表元，不需要进行约化
		if(i<Nqubit){
			for(int i_q=0; i_q<Nqubit; i_q++)
				representative[Nqubit-1-i_q] = Teichmuller[i][d-1-i_q]; //将Teichmuller的后Nqubit个元素交给representative的Nqubit个元素。
		}
		else
			representative = Teichmuller_reduce(Teichmuller[i], primitive, Nqubit);

		representative_list.push_back(representative);
	}
	//将Teichmuller set 元素转化为Galois Field 代表元，见Multicomplementary operator via finite Fourier transform Eq(3.26)
	displayln("representative_list");
	displayln_vector_vector(representative_list);

	/////////////////////////////////////////////////////
	//step 1:求Galois elements trace: 见Eq(3.3)
	//由trace的线性性，我只需要求每个basis 的trace:
	vector<bool> trace_basis(Nqubit,0);
	for(int i=0; i<Nqubit; i++){
		//i 循环求X的第i次方对应的trace 值
		int highest_order = i*ftoi(pow(2,Nqubit_m1));
		vector<bool> trace_poly(1+highest_order,0);
		for(int i_q=0; i_q<Nqubit; i_q++){
			trace_poly[highest_order-i*ftoi(pow(2,i_q))]=(not trace_poly[highest_order-i*ftoi(pow(2,i_q))]);
		}
		//displayln_vector(trace_poly);
		trace_basis[i] = Teichmuller_reduce(trace_poly,primitive,Nqubit).back();
	}
	//displayln("trace_basis");
	//displayln_vector(trace_basis);
	vector<bool> trace_list(d,0);
	int ind =0;
	for(vector<vector<bool> >::iterator it=representative_list.begin(); it!=representative_list.end();it++){
		int ind2=0;
		for(vector<bool>::iterator it2=(*it).begin(); it2 != (*it).end(); it2++){  //根据representative对应次数(0,1...Nqubit-1)的系数是1还是0决定要不要加入这个trace_basis
			trace_list[ind] = trace_list[ind]^((*it2) && trace_basis[Nqubit_m1-ind2]);
			ind2++;
		}
		ind++;
	}
	displayln("trace_list");
	displayln_vector(trace_list);
	/////////////////////////////////////////////////////
	//step 2:求additive character 见Eq(3.5)
	//不用求，trace为1的话character是-1，trace 为0的话character是1
	
	//step 3:deriving list for Zq. Stored in Zq_decomposition_list
	//find the order of 001, 010, 100
	vector<int> order_list;
	for(int i=0; i<Nqubit;i++){
		vector<bool> tmp(Nqubit,0);
		tmp[Nqubit_m1-i]=1;  //i=0 -> tmp==001
		for(int ind=0; ind<d; ind++){
			if(not compare_vectorb(representative_list[ind],tmp))
				order_list.push_back(ind);
		}
	}
	//displayln_vector(order_list);
	vector<string> Zq_decomposition_list;
	for(int q=0; q<d_m1;q++){
		string Zq(Nqubit,'0');
		for(int i=0;i<Nqubit;i++){
			int q_k = (q+order_list[i])%d_m1;
			if(q_k==0)
				q_k=d_m1;
			if(trace_list[q_k]==1)
				Zq[Nqubit_m1-i]='3';
		}
		Zq_decomposition_list.push_back(Zq);
	}
	displayln("Z_q list see Eq(4.15):");
	displayln_vector(Zq_decomposition_list);
	//step 4: IIX, IXI, XII 对应的q就是0，1，2，因此不用求
	//
	//step 5: 计算X_q Z_{q+k}的generators
	//首先输出基础的Z, X的stabilizer,见Eq.(4.15)
	vector<string> stabilizer_X_list;
	for(int i=0; i<Nqubit; i++){
		string generator(Nqubit,'0');
		generator[Nqubit_m1-i]='3';
		stabilizer_X_list.push_back(generator);
	}
	write_stabilizer(stabilizer_X_list,Nqubit);


	for(int i=0; i<Nqubit;i++){
		stabilizer_X_list[i][Nqubit_m1-i]='1';
	}
	write_stabilizer(stabilizer_X_list,Nqubit);

	//见Eq.(4.16)
	displayln("Result:\nstabilizer X_q Z_{q+k}");
	for(int k=0; k<d_m1; k++){
	//stabilizer循环
		vector<string> stabilizer_list;
		for(int q=0; q<Nqubit; q++){
			//generator 循环
			//X_q Z_{q+k}
			string generator=Pauli_merge(stabilizer_X_list[q], Zq_decomposition_list[ (q+k)%d_m1 ],Nqubit);//做pauli乘积
			stabilizer_list.push_back(generator);
		}
		displayln_vector(stabilizer_list);
		write_stabilizer(stabilizer_list,Nqubit);
	}


	return 0;

}
