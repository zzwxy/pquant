#include "pquant.h"
using namespace pquant;
using namespace Eigen;
using namespace std;

int main(){
	
	int Nqubit =2;
	
	bool if_show=true;
	sVector r=sVector::LinSpaced(ftoi(pow(2,Nqubit)),1,ftoi(pow(2,Nqubit)));

	quantum_circuit QC(Nqubit,r);
	if(if_show==true)
		QC.show_state_vector();
	QC.X(0);
	if(if_show==true)
		QC.show_state_vector();
	QC.Y(0);
	if(if_show==true)
		QC.show_state_vector();
	QC.Z(0);
	if(if_show==true)
		QC.show_state_vector();
	QC.H(0);
	if(if_show==true)
		QC.show_state_vector();

	QC.CX(0,1);
	if(if_show==true)
		QC.show_state_vector();
	QC.RX(M_PI/3,0);
	if(if_show==true)
		QC.show_state_vector();
	QC.RY(M_PI/3,0);
	if(if_show==true)
		QC.show_state_vector();
	QC.RZ(M_PI/3,0);
	if(if_show==true)
		QC.show_state_vector();
	//QC.Random_Pauli();
	//QC.Random_Pauli();
	if(if_show==true)
		QC.show_state_vector();

	if(if_show==true)
		QC.show_density_matrix();

	QC.show_all_gate_list();
	QC.show_logic_gate_list();




	return 0;
}
