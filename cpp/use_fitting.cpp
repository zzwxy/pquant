#include<iostream>
#include "fitting.h"
//#include "show.h"
#include<vector>
using namespace std;
using namespace pquant;
int main(){

	Fit fit;

	vector<double> x{0,-1,-2};
	vector<double> y{0,1,2};
	fit.linearFit(x,y);
	double a = fit.getFactor(0);
	double b = fit.getFactor(1);
	cout<<"a = "<<a<< " b = "<<b<<endl;
	return 0;
}
