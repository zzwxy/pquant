
#include "pquant.h"


using namespace pquant;
using namespace Eigen;
using namespace std;


int main(){

	int Nqubit =2;
	
	sVector r=sVector::LinSpaced(ftoi(pow(2,Nqubit)),1,ftoi(pow(2,Nqubit)));


	noise_model NM(Nqubit);

	cout<<NM.N<<endl;

	pair<string,float> pgsf1("Y",0.5);
	pair<string,float> pgsf0("Id",0.5);


	vector<pair<string,float> > noise_info{pgsf0,pgsf1};
	

	NM.clear_noise_model();

	NM.add_all_qubit_quantum_noise("u2",noise_info);

	NM.display_noise_model_map(true);

	quantum_circuit QC(Nqubit,r);
	QC.implement_noise(NM.noise_model_map);
	

	bool if_show=true;
	if(if_show==true)
		QC.show_state_vector();
	QC.X(0);
	if(if_show==true)
		QC.show_state_vector();
	QC.Y(0);
	if(if_show==true)
		QC.show_state_vector();
	QC.Z(0);
	if(if_show==true)
		QC.show_state_vector();
	QC.H(0);
	if(if_show==true)
		QC.show_state_vector();

	QC.CX(0,1);
	if(if_show==true)
		QC.show_state_vector();
	QC.RX(M_PI/3,0);
	if(if_show==true)
		QC.show_state_vector();
	QC.RY(M_PI/3,0);
	if(if_show==true)
		QC.show_state_vector();
	QC.RZ(M_PI/3,0);
	if(if_show==true)
		QC.show_state_vector();
	//QC.Random_Pauli();
	//QC.Random_Pauli();
	if(if_show==true)
		QC.show_state_vector();

	if(if_show==true)
		QC.show_density_matrix();

	QC.show_all_gate_list();
	QC.show_logic_gate_list();


	return 0;
}
