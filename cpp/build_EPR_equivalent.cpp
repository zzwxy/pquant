#include "pquant.h"
using namespace pquant;
using namespace Eigen;
using namespace std;

int main(){
	
	int Nqubit =3;
	
	bool if_show=true;

	quantum_circuit QC(Nqubit);
	if(if_show==true)
		QC.show_state_vector();
	for(int i=0;i<Nqubit;i++)
		QC.H(i);
	if(if_show==true)
		QC.show_state_vector();
	QC.CX(0,1);
	if(if_show==true)
		QC.show_state_vector();
	QC.Z(1);
	QC.Z(2);

	for(int i=0;i<Nqubit;i++)
		QC.P(i);

	if(if_show==true)
		QC.show_state_vector();
	QC.CX(0,1);
	if(if_show==true)
		QC.show_state_vector();

	QC.P(1);
	QC.P(2);
	if(if_show==true)
		QC.show_state_vector();

	QC.CX(2,1);
	if(if_show==true)
		QC.show_state_vector();
	QC.H(1);
	QC.H(2);
	if(if_show==true)
		QC.show_state_vector();



	QC.show_all_gate_list();
	QC.show_logic_gate_list();




	return 0;
}
