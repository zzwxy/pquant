#include "pquant.h"


using namespace pquant;
using namespace Eigen;
using namespace std;

int main(){

	


	vector<int> Nqubit_list{2};
	int len_qubit = Nqubit_list.size();
		  
	int Nsample_list[]={10000};
	int Nlayer_list[] ={6};
	int i_Nqubit=0;

	for(vector<int>::iterator it=Nqubit_list.begin(); it!=Nqubit_list.end(); it++){


		int Nqubit=*it;
		
		const sVector r=sVector::LinSpaced(ftoi(pow(2,Nqubit)),1,ftoi(pow(2,Nqubit)));


		int d = ftoi(pow(2,Nqubit));
		
		quantum_circuit qc(Nqubit,r);

		qc.show_density_matrix();
		sDensity density_matrix = sDensity::Zero(d,d);

		for(int i_sample=0; i_sample<Nsample_list[i_Nqubit]; i_sample++){
			//reset the initial state vector to r vector;
			qc.state_reset(r);

			MatrixXd random_parameter = MatrixXd::Random(Nqubit,2*Nlayer_list[i_Nqubit]);
			for(int i_q=0; i_q< Nqubit; i_q++){
				for(int i_l=0; i_l<Nlayer_list[i_Nqubit]; i_l++){
				qc.RZ(random_parameter(i_q,0+2*i_l),i_q);
				qc.RX(random_parameter(i_q,1+2*i_l),i_q);
				}
			}

			density_matrix+=qc.get_density_matrix();

		}
		density_matrix/=(double)Nsample_list[i_Nqubit];

		cout<<"sum density matrix = "<<endl<<density_matrix<<endl;

		SelfAdjointEigenSolver<sDensity > eigensolver(density_matrix);
		if (eigensolver.info() != Success) abort();
		Matrix<Complex,Dynamic,1> eigen_values = eigensolver.eigenvalues();//Eigen不允许数据类型之间的转换，double到complex<double>都不行，这个设定将会导致编译时Eigen源程序报错，而且你根本不知道你哪错了
		cout<<"eigenvalues = "<<eigen_values<<endl;
		Complex entropy = -eigen_values.transpose()*eigen_values.array().log2().matrix();
		cout<<"Qubit "<<Nqubit<<" entropy = "<<entropy<<endl;

		i_Nqubit++;

		
	}
	
}

