
#include "pquant.h"
using namespace pquant;
using namespace Eigen;
using namespace std;

int main(){
	
	int Nqubit =7;
	
	bool if_show=false;
	sVector r=sVector::LinSpaced(ftoi(pow(2,Nqubit)),1,ftoi(pow(2,Nqubit)));

	quantum_circuit QC(Nqubit,r);
	if(if_show==true)
		QC.show_state_vector();
	QC.RX(M_PI/3,0);

	QC.X(0);
	QC.Random_Pauli();
	QC.Random_Pauli();
	if(if_show==true)
		QC.show_state_vector();

	if(if_show==true)
		QC.show_density_matrix();

	QC.show_gate_list();




	return 0;
}
